## @package happy.cut
#  @brief Classes to replace TCut with extended functionality.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.stringTools import removeRedundantParantheses
import hashlib

## Class to represent cuts or weight expressions.
#  Provides intuitive implementation for logical combination of multiple cuts
#  via operator overloading. Supports implicit conversion from plain strings
#  and TCut objects to Cut, i.e. in all places where Cut is expected a simple
#  string or a TCut object can be passed as well.
#
#  Supported operators:
#    - "-": logical NOT, i.e. inverts the cut
#    - "+" or "&": logical AND of two cuts
#    - "|": logical OR of two cuts
#    - "*": product of two cuts
#
#  @note two Cut objects are considered equal if their cut expression is equal
class Cut( object ):
    ## String that is added to the title when inverting a Cut
    stringNOT   = 'NOT '
    ## String that is added between the two titles of two Cut objects that are added
    stringAND   = ' AND '
    ## String that is added between the two titles of two Cut objects that are or'ed
    stringOR    = ' OR '
    ## String that is added between the two titles of two Cut objects that are multiplied
    stringTIMES = ' X '
    
    ## Default constructor.
    #  If title and/or cut are empty the name is used respectively.
    #  @param name    name of the cut (used for file names etc.: avoid special characters)
    #  @param title   title of the cut (used for legend titles etc.: use TLatex)
    #  @param cut     the actual cut string (see TTree::Draw)
    def __init__( self, name='', title=None, cut=None ):
        from ROOT import TCut
        if isinstance( name, TCut ):
            ## name of the cut (use for filenames, etc.)
            self.name = name.GetName()
            ## title of the cut (use for legend titles, etc.)
            self.title = name.GetName()
            ## cut expression (used for TTree::Draw or similar)
            self.cut = name.GetTitle()
        else:
            if name is None:
                name = ''
            elif not isinstance(name, basestring):
                name = str(name)
            self.name = name
            self.title = title if title is not None else name
            self.cut = cut if cut is not None else name
    
    ## Constructor from a ROOT::TCut.
    #  The name of the TCut object is used both for name and title.
    #  The title of the TCut object is used as the cut string.
    #  @return Cut object
    @classmethod
    def fromTCut( cls, cut ):
        title = cut.GetName()
        return cls( title, title, cut.GetTitle() )

    ## Constructor from an XML element.
    #  @code
    #  <Cut name="CutName" title=CutTitle> CutString </Cut>
    #  @endcode
    #  @param element    the XML element
    #  @return Cut object
    @classmethod
    def fromXML( cls, element ):
        cut = element.text.strip() if element.text else ''
        name = element.attrib['name'] if element.attrib.has_key( 'name' ) else cut
        title = element.attrib['title'] if element.attrib.has_key( 'title' ) else ''
        if CUTS.has_key( name ):
            return CUTS[ name ]
        return cls(name, title, cut)

    @property
    def cut( self ):
        return self.__cut
    
    @cut.setter
    def cut( self, cut ):
        # remove white spaces
        self.__cut = removeRedundantParantheses( cut.replace( ' ', '' ) )

    def __repr__( self ):
        return 'Cut(%r, %r, %r)' % (self.name, self.title, self.cut)
    
    def __str__( self ):
        return self.title

    ## Convenience method for changing name and title in one line.
    #  @param name    new name of the cut
    #  @param title   new title of the cut
    #  @return self
    def setNameTitle( self, name='', title='' ):
        if name:
            self.name = name
        if title:
            self.title = title
        return self

    ## Create a corresponding TCut object.
    #  @return TCut
    def getTCut( self ):
        from ROOT import TCut
        return TCut( self.title, self.cut )

    ## Create a corresponding TTreeFormula object.
    #  @param tree    TTree (optional)
    #  @return TTreeFormula
    def getTTreeFormula( self, tree=0 ):
        from ROOT import TTreeFormula
        import warnings
        warnings.filterwarnings( action='ignore', category=RuntimeWarning, message='creating converter.*' )
        if self.cut:
            return TTreeFormula( self.name, self.cut, tree )
        else:
            return TTreeFormula( self.name, '1', tree )

    ## Create a new Cut which ignores part of the current cut expression
    #  @attention Uses string replacement, so the substring has to be an exact match.
    #  @param other    Cut
    #  @return Cut without the other cut expression
    def withoutCut( self, other ):
        if not isinstance( other, Cut ):
            other = Cut( other )
        formula = self.cut.replace(other.cut, '')
        formula = self.__cleanFormula(formula)
        return Cut( '%s_WITHOUT_%s' % (self.name, other.name), '%s WITHOUT %s' % (self.title, other.title), self.cut.replace(other.cut, '1') )
    
    def __cleanFormula( self, cutFormula):
        if '()' in cutFormula:
            parts = cutFormula.split( '()' )
            newFormula = None
            for er in range( len(parts)-1 ):
                # removing '||)' and '&&)' if any
                if parts[er][-2:] in [ '&&', '||' ] and parts[er+1][:1] in [')']:
                    if not newFormula:  newFormula = parts[er][:-2] + parts[er+1]
                    else:               newFormula = newFormula + parts[er][:-2] + parts[er+1]
                # removing '(||' and '(&&' if any
                if parts[er+1][:2] in [ '&&', '||' ] and parts[er][-1:] in ['(']:
                    if not newFormula:  newFormula = parts[er] + parts[er+1][2:]
                    else:               newFormula = newFormula + parts[er] + parts[er+1][2:]
                # Take in account exceptions like lep.Eta()
                if('.' in parts[er]):
                    if not newFormula:  newFormula = parts[er] + '()' + parts[er+1]
                    else:               newFormula = newFormula + '()' + parts[er+1]
            cutFormula = newFormula

        return cutFormula

    def isSubset( self, other ):
        # TODO
        pass
    
    def isSuperset( self, other ):
        #TODO
        pass

    ## Creates a new Cut that is the logical NOT of this Cut.
    #  @param name    name of the new Cut
    #  @param title   title of the new Cut
    #  @return Inverted Cut
    def NOT( self, name='', title='' ):
        return (-self).setNameTitle( name , title )

    ## Creates a new Cut that is the logical AND of this and the other Cut.
    #  @param other   other Cut
    #  @param name    name of the new Cut
    #  @param title   title of the new Cut
    #  @return Cut AND other Cut
    def AND( self, other, name='', title='' ):
        return (self & other).setNameTitle( name , title )
    
    ## Creates a new Cut that is the logical OR of this and the other Cut.
    #  @param other   other Cut
    #  @param name    name of the new Cut
    #  @param title   title of the new Cut
    #  @return Cut OR other Cut
    def OR( self, other, name='', title='' ):
        return (self | other).setNameTitle( name , title )

    ## Creates a new Cut that is the product of this and the other Cut.
    #  Corresponds to a logical AND including possible weights.
    #  @param other   other Cut
    #  @param name    name of the new Cut
    #  @param title   title of the new Cut
    #  @return Cut * other Cut
    def TIMES( self, other, name='', title='' ):
        return (self*other).setNameTitle( name , title )

    # Implement == operator.
    def __eq__( self, other ):
        if not isinstance( other, Cut ):
            other = Cut( other )
        return self.cut == other.cut
    
    # Implement hash function.
    def __hash__( self ):
        return hash( self.cut )
    
    ## Generate an md5 checksum.
    #  Only considers the cut expression string and not name or title.
    @property
    def md5( self ):
        return hashlib.md5( self.cut ).hexdigest()

    # implement -self operator: logical NOT.
    def __neg__( self ):
        if not self.cut:
            return self
        return Cut( 'NOT_%s' % self.name, '%s%s' % (self.stringNOT, self.title), '!(%s)' % self.cut )

    # implement & operator: logical AND.
    def __and__( self, other ):
        if not isinstance( other, Cut ):
            other = Cut( other )
        if not other.cut:
            return self
        if not self.cut:
            return other
        return Cut( '%s_AND_%s' % (self.name, other.name), '%s%s%s' % (self.title, self.stringAND, other.title), '(%s) && (%s)' % (self.cut, other.cut) )
    
    # implement | operator: logical OR.
    def __or__( self, other ):
        if not isinstance( other, Cut ):
            other = Cut( other )
        if not other.cut:
            return self
        if not self.cut:
            return other
        return Cut( '%s_OR_%s' % (self.name, other.name), '%s%s%s' % (self.title, self.stringOR, other.title), '(%s) || (%s)' % (self.cut, other.cut) )

    # implement * operator: logical AND (with weights).
    def __mul__( self, other ):
        if not isinstance( other, Cut ):
            other = Cut( other )
        if not other.cut:
            return self
        if not self.cut:
            return other
        return Cut( '%s_X_%s' % (self.name, other.name), '%s%s%s' % (self.title, self.stringTIMES, other.title), '(%s) * (%s)' % (self.cut, other.cut) )

    # implement + operator: logical AND.
    def __add__( self, other ):
        return self & other
    
    # implement - operator: logical AND NOT.
    def __sub__( self, other ):
        return self & -other
    
    # implement cut &= other
    def __iand__( self, other ):
        self = self & other
        return self
    
    # implement cut |= other
    def __ior__( self, other ):
        self = self | other
        return self
    
    # implement cut *= other
    def __imul__( self, other ):
        self = self * other
        return self
    
    # implement cut += other
    def __iadd__( self, other ):
        self = self + other
        return self
    
    # implement cut -= other
    def __isub__( self, other ):
        self = self - other
        return self
    
    # implicit conversion to Cut for &
    def __rand__( self, other ):
        return Cut( other ) & self
    
    # implicit conversion to Cut for |
    def __ror__( self, other ):
        return Cut( other ) | self
    
    # implicit conversion to Cut for *
    def __rmul__( self, other ):
        return Cut( other ) * self
    
    # implicit conversion to Cut for +
    def __radd__( self, other ):
        return Cut( other ) + self
    
    # implicit conversion to Cut for -
    def __rsub__( self, other ):
        return Cut( other ) - self
    
    
if __name__ == '__main__':
    from ROOT import TCut
    c1 = Cut( 'boosted_region', 'Boosted (M_{vis} > 100GeV)', 'ditau_vis_mass > 100.' )
    c2 = Cut( 'rho-rho_decay', '#rho#rho-decay', 'ditau_tau0_decay_mode*ditau_tau1_decay_mode==1' )
    
    print 'Define two cut objects:'
    print '\tc1:\t', c1
    print '\tc2:\t', c2
    print
    print 'Standard logical operations:'
    print '\tNOT c1:\t\t',    -c1
    print '\tc1 AND c2:\t',    c1 & c2
    print '\tc1 OR c2:\t',     c1 | c2
    print '\tc1 TIMES c2:\t',  c1 * c2
    print 
    print 'Logical operations with implicit renaming:'
    print '\tc1 AND c2:\t',      c1.AND( c2, 'signal_region', 'Signal Region' )
    print '\tNOT(c1 AND c2):\t', (c1 & c2).NOT('control_region', 'Control Region')
    print
    print 'Implicit conversion of plain strings and TCut:'
    print '\t"str" AND c1:\t',   '!selection_vbf' + c1
    print '\tc1 OR TCut:\t',     c1 | TCut( 'VBF Region', 'selection_vbf' )
    
    print Cut( 'a || (b < 70)' ) == 'a || (b < 70)'
    print Cut( 'a || (b < 70)' ) == '(b < 70) || a'
