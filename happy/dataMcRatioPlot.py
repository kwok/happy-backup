from happy.plot import LegendElement
from happy.plot import Plot
from happy.doublePlot import DoublePlot, __generatePlotUpInterface__
from happy.dataMcPlot import DataMcPlot
from happy.variable import Binning, Variable, var_Events, var_Ratio
from happy.histogramTools import histToGraph, addGraphs, resetGraph, divideGraphs, SoverSqrtSplusBGraph, SoverSqrtSplusBHistogram
from happy.miscellaneous import Enum
from happy.stringTools import appendUUID
from copy import copy
import logging

var_dataMc = Variable( 'Data / Model', binning=Binning(low=0.7, high=1.3) )
var_dataMc.binning.nDivisions = 205

## Helper base class that holds different modes. 
class HasModes( object ):
    modes = Enum()
    
    @property
    def mode( self ):
        return self._mode
    
    @mode.setter
    def mode( self, value ):
        if value not in self.modes:
            raise ValueError( '%r is invalid for %s.mode. Allowed modes are %r' %(value, self.__class__.__name__, self.modes) )
        self._mode = value

## Plot that is filled by ratios or subtraction of histograms wrt. a reference.
#  The ratios are calculated during the draw step, i.e. if the histograms or graphs
#  and their corresponding references are only finalised during the draw step they
#  will be taken into account correctly.
#  Also allows to subtract the reference instead of dividing by the reference.
class RatioPlot( Plot, HasModes ):
    logger = logging.getLogger( __name__ + '.RatioPlot' )
    modes = Enum( ['SUBTRACT', 'DIVIDE', 'S_OVER_SQRT_S_PLUS_B'] )
    
    ## Default constuctor.
    #  @param title       string used as title of the TCanvas
    #  @param variableX   Variable object defining the x axis
    #  @param variableY   Variable object defining the y axis (optional, default uses "Ratio")
    def __init__( self, title='RatioPlot', variableX=None, variableY=var_Ratio ):
        Plot.__init__( self, title, variableX, variableY )
        self._ratioHistograms = {}
        ## Dictionary 
        self._ratioGraphs = {}
        ## Define which method is used to calculate the ratio
        self.mode = self.modes.DIVIDE
        
    def remove( self, obj ):
        Plot.remove( self, obj )
        # make sure to also remove the corresponding ratios
        for dictionary in [self._ratioGraphs, self._ratioHistograms]:
            if dictionary.has_key( obj ):
                del dictionary[ obj ]
    
    ## Add a ratio of two histograms.
    #  The actual ratio histogram is a copy of the original histogram. It will only be
    #  calculated when draw() is called.
    #  @param histogram     TH1 object that will be divided by the reference
    #  @param reference     TH1 object that acts as the reference
    #  @param drawOption    draw option used for the ratio histogram (optional, default is "HIST")
    #  @param addReference  decide if the reference should be added before dividing (optional, default is False)
    #  @return the ratio histogram (TH1)
    def addRatioHistogram( self, histogram, reference, drawOption='HIST', addReference=False ):
        ratioHist = histogram.Clone( appendUUID( 'Ratio_%s_vs_%s' % ( histogram.GetName(), reference.GetName() ) ) )
        self._ratioHistograms[ratioHist] = (histogram, reference, addReference)
        return self.addHistogram( ratioHist, drawOption, False, False )

    ## Add a ratio of two graphs.
    #  The actual ratio graph is a copy of the original graph. It will only be
    #  calculated when draw() is called.
    #  @param graph          TGraph object that will be divided by the reference
    #  @param reference      TGraph object that acts as the reference
    #  @param drawOption     draw option used for the ratio graph (default is "P")
    #  @param addReference  decide if the reference should be added before dividing (optional, default is False)
    #  @return the ratio graph (TGraphAsymmErrors)
    def addRatioGraph( self, graph, reference, drawOption='P', addReference=False ):
        ratioGraph = graph.Clone( appendUUID( 'Ratio_%s_vs_%s' % ( graph.GetName(), reference.GetName() ) ) )
        self._ratioGraphs[ratioGraph] = (graph, reference, addReference)
        return self.addGraph( ratioGraph, drawOption )
    
    def __mainAction__( self ):
        Plot.__mainAction__(self)
        for ratioHist, value in self._ratioHistograms.iteritems():
            hist, reference, addReference = value
            ratioHist.Reset()
            ratioHist.Add( hist )
            if addReference:
                ratioHist.Add( reference )
            if self.mode is self.modes.SUBTRACT:
                ratioHist.Add( reference, -1. )
            elif self.mode is self.modes.S_OVER_SQRT_S_PLUS_B:
                SoverSqrtSplusBHistogram( ratioHist, reference )
            else:
                ratioHist.Divide( reference )
        for ratioGraph, value in self._ratioGraphs.iteritems():
            graph, reference, addReference = value
            resetGraph( ratioGraph )
            addGraphs( ratioGraph, graph )
            if addReference:
                addGraphs( ratioGraph, reference )
            if self.mode is self.modes.SUBTRACT:
                addGraphs( ratioGraph, reference, -1. )
            elif self.mode is self.modes.S_OVER_SQRT_S_PLUS_B:
                SoverSqrtSplusBGraph( ratioGraph, reference )
            else:
                divideGraphs( ratioGraph, reference )

class DataMcRatioPlot( DoublePlot, DataMcPlot, HasModes ):
    from happy.style import Style
    defaultBaseLineStyle = Style( 922, lineWidth=2, lineStyle=2, markerStyle=0 )
    # TODO: implement cases for 'INT_S_OVER_SQRT_S_PLUS_B'
    modes = Enum( ['DATA_OVER_MODEL', 'DATA_MINUS_MODEL', 'S_OVER_B', 'S_OVER_SQRT_S_PLUS_B'] )
    ## Default constructor.
    #  @param title          title of the canvas
    #  @param variableX      Variable object defining the x axis
    #  @param variableYUp    Variable object defining the y axis in the upper plot (optional, default uses "Entries")
    #  @param variableYDown  Variable object defining the y axis, in the lower plot (optional, default used "Ratio")
    #  @param upperSize      relative size of the upper plot wrt. the lower plot (optional, default is 3)
    #  @param lowerSize      relative size of the upper plot wrt. the lower plot (optional, default is 1)
    def __init__( self, title='DataMcRatioPlot', xVariable=None, yVariableUp=var_Events, yVariableDown=var_dataMc, upperFraction=0.62, lowerFraction=0.38 ):
        DoublePlot.__init__( self, title, xVariable, yVariableUp, yVariableDown, upperFraction, lowerFraction )
        self.plots[0][0] = RatioPlot( self.plots[0][0].title, xVariable, yVariableDown )
        self.plots[0][1] = DataMcPlot( self.plots[0][1].title, xVariable, yVariableUp )
        # ratio plot should not have a legend
        self.plotDown.drawTitle = False
        self.plotDown.drawLegend = False
        self.plotDown.showBinWidthY = False
        ## settings from default settings 
        self.baseLineStyle = copy( self.defaultBaseLineStyle )
        ## Graph that represents the data-MC ratio
        self._dataMcRatioGraph = None
        ## Graph that represents the combined background uncertainty divided by the nominal
        self._mcCombinedErrorRatioGraph = None
        ## Graph that represents the systematic background uncertainty divided by the nominal
        self._mcSystRatioGraph = None
        ## Graph that represents the statistical background uncertainty divided by the nominal
        self._mcSumRatioHist = None
        ## Baseline indication in the ratio plot
        self.baseLine = None
        # flags to decide which errors bars are shown
        ## Show the statistical uncertainty also in the ratio plot
        self.drawStatErrorDown = True
        ## Show the systematic uncertainty also in the ratio plot
        self.drawSystErrorDown = False
        ## Show the combined uncertainty band also in the ratio plot
        self.drawCombinedErrorDown = True
        ## Add small arrows to indicate data points that are out of the y-axis range in the ratio plot
        self.drawOutOfRangeArrows = False
        ## Mode to decide what is shown in the ratio plot
        self.mode = DataMcRatioPlot.modes.DATA_OVER_MODEL
    
    def draw( self ):
        # remove potential legend entries from earlier calls
        for obj in [ self._mcSumRatioHist, self._mcSystRatioGraph, self._mcCombinedErrorRatioGraph ]:
            self.remove( obj )
        # remove objects from the ratio plot
        for obj in [ self._mcSumRatioHist, self._mcSystRatioGraph, self._mcCombinedErrorRatioGraph, self._dataMcRatioGraph, self.baseLine ]:
            self.plotDown.remove( obj )
        # register ratio objects with the lower plot depending on the options
        if self._mcSystGraph and self.drawCombinedErrorDown and self.mode not in [self.modes.S_OVER_B, self.modes.S_OVER_SQRT_S_PLUS_B]:    
            self._mcCombinedErrorRatioGraph = self.plotDown.addRatioGraph( self._mcCombinedErrorGraph, self._mcSumHistWithoutErrors, '5' )
            self.combinedErrorStyle.applyTo( self._mcCombinedErrorRatioGraph )
            self._mcCombinedErrorRatioGraph.SetTitle( self.combinedErrorTitle )
        if self._mcSystGraph and self.drawSystErrorDown and self.mode not in [self.modes.S_OVER_B, self.modes.S_OVER_SQRT_S_PLUS_B]:
            self._mcSystRatioGraph = self.plotDown.addRatioGraph( self._mcSystGraph, self._mcSumHistWithoutErrors, '5' )
            self.systErrorStyle.applyTo( self._mcSystRatioGraph )
            self._mcSystRatioGraph.SetTitle( self.systErrorTitle )
        if self._mcSumHist and self.drawStatErrorDown and self.mode not in [self.modes.S_OVER_B, self.modes.S_OVER_SQRT_S_PLUS_B]:
            self._mcSumRatioHist = self.plotDown.addRatioHistogram( self._mcSumHist, self._mcSumHistWithoutErrors, 'E2' )
            self.statErrorStyle.applyTo( self._mcSumRatioHist )
            self._mcSumRatioHist.SetTitle( self.statErrorTitle )
        # add the signal histograms
        signalRatioHists = []
        for signalHist in self.signalHistograms:
            if self.mode in [self.modes.S_OVER_B, self.modes.S_OVER_SQRT_S_PLUS_B]:
                addReference = False
            else:
                addReference = True
            signalRatioHists.append( self.plotDown.addRatioHistogram( signalHist, self._mcSumHistWithoutErrors, 'Hist', addReference ) )
        # add the baseline for the ratio plot
        if self.variableX:
            from ROOT import TF1
            yValue = 0.
            if self.mode in [self.modes.DATA_OVER_MODEL]:
                yValue = 1.0
            self.baseLine = self.plotDown.addFunction( TF1( '%s_baseLine' % self.title, str(yValue), self.variableX.binning.low, self.variableX.binning.high ) )
            self.baseLineStyle.applyTo( self.baseLine )
        # add the data ratio if applicable
        if self._dataGraph and self.mode not in [self.modes.S_OVER_B, self.modes.S_OVER_SQRT_S_PLUS_B]:
            self._dataMcRatioGraph = self.plotDown.addRatioGraph( self._dataGraph, self._mcSumHistWithoutErrors, 'P0' )
        # add out of range arrows
        if self.drawOutOfRangeArrows:
            self.plotDown.addOutOfRangeArrows( self._dataMcRatioGraph )
        # add legend entries in upper plot for items that are only drawn in ratio plot
        if not self.drawStatError and self.drawStatErrorDown and self._mcSumRatioHist:
            self.legendElements.append( LegendElement( self._mcSumRatioHist, self.statErrorTitle, 'f' ) )
        if not self.drawSystError and self.drawSystErrorDown and self._mcSystRatioGraph:
            self.legendElements.append( LegendElement( self._mcSystRatioGraph, self.systErrorTitle, 'f' ) )
        if not self.drawCombinedError and self.drawCombinedErrorDown and self._mcCombinedErrorRatioGraph:
            self.legendElements.append( LegendElement( self._mcCombinedErrorRatioGraph, self.combinedErrorTitle, 'f' ) )
        if self.mode in [self.modes.DATA_MINUS_MODEL]:
            self.plotDown.mode = RatioPlot.modes.SUBTRACT
        elif self.mode in [self.modes.S_OVER_SQRT_S_PLUS_B]:
            self.plotDown.mode = RatioPlot.modes.S_OVER_SQRT_S_PLUS_B
        else:
            self.plotDown.mode = RatioPlot.modes.DIVIDE
        DoublePlot.draw( self )
        for signalRatioHist in signalRatioHists:
            self.plotDown.remove( signalRatioHist )

__generatePlotUpInterface__( DataMcRatioPlot(), DataMcPlot() )        
                
if __name__ == '__main__':
    from ROOT import TH1D, TF1, kBlack, kRed, kBlue, kGreen, kViolet, kOrange
    from happy.style import mcErrorStyle
    from happy.plotDecorator import AtlasTitleDecorator
    
    fFlat = TF1( 'f_flat', '1' )
    fZ = TF1( 'f_Z', 'gaus' )
    fZ.SetParameter( 0, 1. )
    fZ.SetParameter( 1, 91. )
    fZ.SetParameter( 2, 20. )
    fH125 = TF1( 'f_H125', 'gaus' )
    fH125.SetParameter( 0, 1. )
    fH125.SetParameter( 1, 125. )
    fH125.SetParameter( 2, 5. )
    fH150 = TF1( 'f_H150', 'gaus' )
    fH150.SetParameter( 0, 1. )
    fH150.SetParameter( 1, 150. )
    fH150.SetParameter( 2, 5. )
    
    xVar = Variable( 'Invariant Mass', unit='GeV', binning=Binning( 24, 50, 170 ) )
    
    h1 = xVar.createHistogram( 'Z background' )
    h1.FillRandom( 'f_Z', 10000 )
    h1.Scale(0.5)
    h1.SetLineColor( kBlue-9 )
    
    h2 = xVar.createHistogram( 'Flat background' )
    h2.FillRandom( 'f_flat', 10000 )
    h2.Scale(0.2)
    h2.SetLineColor( kViolet+2 )   # if no fill color is defined, the line color is used to fill stacked histograms
    
    h3 = xVar.createHistogram( 'Signal 125' )
    h3.FillRandom( 'f_H125', 10000 )
    h3.Scale(0.015)
    h3.SetLineWidth( 3 )
    h3.SetLineColor( kRed )   # if no fill color is defined, the line color is used to fill stacked histograms
    
    h4 = xVar.createHistogram( 'Signal 150' )
    h4.FillRandom( 'f_H150', 10000 )
    h4.Scale(0.015)
    h4.SetLineWidth( 3 )
    h4.SetLineColor( kOrange+1 )   # if no fill color is defined, the line color is used to fill stacked histograms
    
    h5 = xVar.createHistogram( 'Data' )
    h5.FillRandom( 'f_Z', 5000 )
    h5.FillRandom( 'f_flat', 2000 )
    h5.FillRandom( 'f_H125', 150 )
    h5.SetLineColor( kBlack )
    
    hSum = xVar.createHistogram( 'Stat' )
    mcErrorStyle.applyTo( hSum )
    
    hRatio = xVar.createHistogram( 'Ratio' )
    
    yVarDown = Variable ( 'Data / Model', binning=Binning(low=0.5, high=1.5) )
    
    # Add the ATLAS label to each plot
    Plot.defaultTitleDecorator = AtlasTitleDecorator( 'Internal' )
    
    # use the DataMc class to create the double plot with ratio
    # create a +30% -20% systematics for the first background only
    sys = histToGraph( h1, 'hSyst', False )
    for i in xrange( sys.GetN() ):
        from ROOT import Double
        x, y = (Double(), Double())
        sys.GetPoint( i, x, y )
        sys.SetPointEYlow( i, 0.1*y )
        sys.SetPointEYhigh( i, 0.15*y )
    # now add the nominal value for the second background
    addGraphs( sys, histToGraph( h2, '', False ) )
    
    dataMcTest = DataMcRatioPlot( 'testDataMcPlot', xVar )
    dataMcTest.drawOutOfRangeArrows = True
    dataMcTest.legendDecorator.textSize = 0.03
    dataMcTest.legendDecorator.maxEntriesPerColumn = 7
    dataMcTest.addHistogram( h1 )
    dataMcTest.addHistogram( h2 )
    dataMcTest.setMCSystematicsGraph( sys )
    dataMcTest.setDataHistogram( h5 )
    dataMcTest.addSignalHistogram( h3 )
    dataMcTest.addSignalHistogram( h4 )
    # drawing in default mode (Data / Model)
    dataMcTest.draw()
    dataMcTest.saveAs( 'testDataMcPlot_0.pdf' )
    # drawing in "Data - Model" mode
    dataMcTest.mode = DataMcRatioPlot.modes.DATA_MINUS_MODEL
    dataMcTest.plotDown.variableY = Variable( 'Data - Model' )
    dataMcTest.draw()
    dataMcTest.saveAs( 'testDataMcPlot_1.pdf' )
    # drawing in "S/B" mode
    dataMcTest.mode = DataMcRatioPlot.modes.S_OVER_B
    dataMcTest.plotDown.variableY = Variable( 'S / B' )
    dataMcTest.draw()
    dataMcTest.saveAs( 'testDataMcPlot_2.pdf' )
    # drawing in "S/sqrt(S+B)" mode
    dataMcTest.mode = DataMcRatioPlot.modes.S_OVER_SQRT_S_PLUS_B
    dataMcTest.plotDown.variableY = Variable( 'S / #sqrt{S + B}' )
    dataMcTest.draw()
    dataMcTest.saveAs( 'testDataMcPlot_3.pdf' )
     
    # accessing the sumMC and data histograms from the DataMcPlot ( only after calling draw() )
    from ROOT import TFile
    f = TFile.Open( 'test.root', 'RECREATE' )
    #blackLine.apply( dataMcTest._mcSumHist ) 
    dataMcTest._mcSumHist.Write( 'SumMC' )
    dataMcTest._dataGraph.Write( 'Data' )
    f.Close()
    
    raw_input( 'Continue?' )
    
