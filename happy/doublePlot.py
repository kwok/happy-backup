from happy.plot import Plot, Canvas
from happy.variable import Binning, var_Entries
import logging

## Canvas that is filled with a matrix of plots.
#  All plots in a each column share the x axis and all plots in each row
#  share their y axis. The outer margins and axis labels maintain the same
#  size as if it was a single plot. Text sizes in all sub plots are scaled
#  to the size as if it was a single plot on a single canvas. In addition,
#  the relative size of each column wrt. other columns can be defined
#  (same for rows). There is an option of adding margins in between the
#  plots.
class PlotMatrix( Canvas ):
    logger = logging.getLogger( __name__ + '.PlotMatrix' )
    
    ## Default constructor.
    #  The matrix of plots is defined by two lists of variables, one for x
    #  and the other for the y direction. The minimum number of variables
    #  in each direction is 1.
    #  @param title        title of the canvas
    #  @param variablesX   list of Variable objects defining the columns of plots
    #  @param variablesY   list of Variable objects defining the rows of plots (optional, default is a single column with "Entries" as y axis label)
    #  @param padSizesX    list of pad sizes (optional, number of entries needs to match number of entries in variablesX)
    #  @param padSizesY    list of pad sizes (optional, number of entries needs to match number of entries in variablesY)
    def __init__( self, title, variablesX=[], variablesY=[var_Entries], padSizesX=None, padSizesY=None ):
        Canvas.__init__( self, title )
        ## Dictionary of x index to dictionary of y index to Plot objects
        self.plots = {}
        ## List of relative pad sizes in x direction, automatically normalised in draw()
        self.padSizesX = padSizesX or len(variablesX) * [1.]
        ## List of relative pad sizes in y direction, automatically normalised in draw()
        self.padSizesY = padSizesY or len(variablesY) * [1.]
        ## Margin between all sub plots in x direction given in fraction of canvas size
        self.padMarginX = 0.01
        ## Margin between all sub plots in y direction given in fraction of canvas size
        self.padMarginY = 0.01
        # create sub-plot objects
        for i, variableX in enumerate(variablesX):
            self.plots[i] = {}
            for j, variableY in enumerate(variablesY):
                plot = Plot( title + '_%d_%d' % (i, j), variableX, variableY )
                self.plots[i][j] = plot

    ## Number of plots in x direction
    @property
    def nPlotsX( self ):
        return len(self.plots)
    
    ## Number of plots in y direction
    @property
    def nPlotsY( self ):
        return len(self.plots[0])
    
    def draw( self ):
        Canvas.draw( self )
        # calculate the pad sizes by splitting the total size excluding all margins
        padX = (1. - self.canvas.GetLeftMargin() - self.canvas.GetRightMargin() - (self.nPlotsX-1)*self.padMarginX) / self.nPlotsX
        padY = (1. - self.canvas.GetTopMargin() - self.canvas.GetBottomMargin() - (self.nPlotsY-1)*self.padMarginY) / self.nPlotsY
        # calculate the scaling factors for the different rows and columns
        if len(self.padSizesX) == self.nPlotsX:
            padScaleX = [self.nPlotsX*x/float(sum(self.padSizesX)) for x in self.padSizesX]
        else:
            self.logger.warning( '__initAction__: length of padSizesX does not match number of plots in x direction. No scaling applied.' )
            padScaleX = self.nPlotsX*[1.0]
        if len(self.padSizesY) == self.nPlotsY:
            padScaleY = [self.nPlotsY*x/float(sum(self.padSizesY)) for x in self.padSizesY]
        else:
            self.logger.warning( '__initAction__: length of padSizesY does not match number of plots in y direction. No scaling applied.' )
            padScaleY = self.nPlotsY*[1.0]
        # divide the main canvas, adjust margins of pads and set them as canvases of sub-plots
        self.canvas.Divide( self.nPlotsX, self.nPlotsY )
        localX = 0.
        for iX in xrange(self.nPlotsX):
            # central margins
            leftMargin = 0.5*self.padMarginX
            rightMargin = 0.5*self.padMarginX
            # outer margins
            if iX == 0:
                leftMargin = self.canvas.GetLeftMargin()
            if iX == self.nPlotsX-1:
                rightMargin = self.canvas.GetRightMargin()
            widthX = leftMargin + padX*padScaleX[iX] + rightMargin
            localY = 0.
            for iY in xrange(self.nPlotsY):
                # central margins
                bottomMargin = 0.5*self.padMarginY
                topMargin = 0.5*self.padMarginY
                # outer margins
                if iY == 0:
                    bottomMargin = self.canvas.GetBottomMargin()
                if iY == self.nPlotsY-1:
                    topMargin = self.canvas.GetTopMargin()
                widthY = bottomMargin + padY*padScaleY[iY] + topMargin
                plot = self.plots[iX][iY]
                # set the canvas pointer to the correct pad
                plot.canvas = self.canvas.cd(1+iX+iY*self.nPlotsX)
                # set pad dimensions
                plot.canvas.SetPad( localX, localY, localX+widthX, localY+widthY )
                # scale the margins in case the reference size of the canvas is different
                plot.canvas.SetLeftMargin( leftMargin / self._scaleX )
                plot.canvas.SetRightMargin( rightMargin / self._scaleX )
                plot.canvas.SetBottomMargin( bottomMargin / self._scaleY )
                plot.canvas.SetTopMargin( topMargin / self._scaleY )
                # propagate the reference size to the sub plot
                plot.referenceSizeX = self.referenceSizeX or self.sizeX
                plot.referenceSizeY = self.referenceSizeY or self.sizeY
                localY += widthY
            localX += widthX
        # draw all sub-plots
        for plotColumn in self.plots.values():
            # rows are numbered bottom-up but we want to draw top-down
            for plot in reversed( plotColumn.values() ):
                plot.draw()
        # align the y-axis of all plots in each row
        for iY in xrange(self.nPlotsY):
            binning = self.plots[0][iY].variableY.binning
            if binning.low and binning.high:
                continue
            minimum = float('inf')
            maximum = float('-inf')
            for iX in xrange(self.nPlotsX):
                plot = self.plots[iX][iY]
                if plot.histograms2D:
                    continue
                minimum = min(minimum, self.plots[iX][iY]._minimum)
                maximum = max(maximum, self.plots[iX][iY]._maximum)
            for iX in xrange(self.nPlotsX):
                obj = self.plots[iX][iY]._firstDrawnObject
                if plot.histograms2D or not obj:
                    continue
                obj.GetYaxis().SetRangeUser( minimum, maximum )
        # remove the axis labels and titles for all axes not on left or bottom
        for iY in xrange(self.nPlotsY):
            for iX in xrange(self.nPlotsX):
                obj = self.plots[iX][iY]._firstDrawnObject
                if iX != 0:
                    yAxis = obj.GetYaxis()
                    yAxis.SetLabelSize( 0 )
                    yAxis.SetTitleSize( 0 )
                if iY != 0:
                    xAxis = obj.GetXaxis()
                    xAxis.SetLabelSize( 0 )
                    xAxis.SetTitleSize( 0 )

## Special version of PlotMatrix with two plots on top of each other.
#  Both plots share a single x axis, i.e. as used for the usual ratio plots. By default the
#  upper plot is 3 times larger than the lower plot which results in a decent looking ratio
#  band on a 600x600 canvas. For a canvas of 600x800 a good choice is 474:192 for the upper
#  and lower sizes which results in an upper plot filling exactly the same area as a single
#  plot on a 600x600 canvas, if the margin between the pads is set to the default value of
#  0.01. If the margin is set to 0 the sizes should be set to 474:200.
#  While inheriting from Plot it is not a fully valid instance of Plot itself. All relevant
#  calls are forwarded to the upper plot object instead. This means, for example,
#  myDoublePlot.plotUp.addHistogram( hist ) and myDoublePlot.addHistogram( hist ) have
#  identical effects. Same goes for the member variables: myDoublePlot.plotUp.histograms is
#  identical to myDoublePlot.histograms. On the other hand, the attributes and methods
#  inherited from Canvas (through PlotMatrix) are not forwarded to the upper plot object as
#  they discribe the main TCanvas that holds the TPads of the upper and lower plot.
class DoublePlot( PlotMatrix, Plot ):
    ## Default constructor.
    #  @param title          title of the canvas
    #  @param variableX      Variable object defining the x axis
    #  @param variableYUp    Variable object defining the y axis in the upper plot (optional, default uses "Entries")
    #  @param variableYDown  Variable object defining the y axis, in the lower plot (optional, default used "Ratio")
    #  @param upperSize      relative size of the upper plot wrt. the lower plot (optional, default is 3)
    #  @param lowerSize      relative size of the upper plot wrt. the lower plot (optional, default is 1)
    def __init__( self, title, variableX, variableYUp=var_Entries, variableYDown=var_Entries, upperSize=3, lowerSize=1 ):
        PlotMatrix.__init__( self, title, (variableX,), (variableYDown, variableYUp), padSizesY=(lowerSize, upperSize) )
        # override some of the default settings
        self.plotDown.drawTitle = False
        self.plotDown.drawLegend = False
        self.plotDown.showBinWidthY = False

    ## Access to the upper Plot object.
    @property
    def plotUp( self ):
        return self.plots[0][1]
    
    ## Access to the lower Plot object.
    @property
    def plotDown( self ):
        return self.plots[0][0]

    ## Access to size of the upper plot
    @property
    def lowerSize( self ):
        return self.padSizesY[0]

    @lowerSize.setter
    def lowerSize( self, value ):
        self.padSizesY[0] = value

    ## Access to size of the upper plot
    @property
    def upperSize( self ):
        return self.padSizesY[1]

    @upperSize.setter
    def upperSize( self, value ):
        self.padSizesY[1] = value
    
# Nested helper class to encapsulate calls to attributes of the upper plot.    
class PlotUpAttribute( object ):
    def __init__( self, attribute ):
        self.attribute = attribute
         
    def __get__( self, instance, owner ):
        return getattr( instance.plotUp, self.attribute )
             
    def __set__( self, instance, value ):
        setattr( instance.plotUp, self.attribute, value )

# helper method to forward Plot attribute calls to the upper plot.
def __generatePlotUpInterface__( plot, nestedPlot ):
    for var in vars( nestedPlot ).keys():
        if var not in vars( plot ).keys():
            setattr( plot.__class__, var, PlotUpAttribute( var ) )

# forward all plot attributes to the upper plot. Needs concrete instances to have all attributes available. 
__generatePlotUpInterface__( DoublePlot( '', None, None, None), Plot() )

def testMultiPlot():
    from ROOT import TH1D, TF1, kBlack, kRed, kBlue, kGreen
    from happy.variable import Variable
    h1 = TH1D( 'hTest1', 'First Test', 20, -4, 4 )
    h1.FillRandom( 'gaus', 1000 )
    h1.SetLineColor( kRed )
    
    h2 = TH1D( 'hTest2', 'Second Test', 20, -4, 4 )
    h2.FillRandom( 'gaus', 500 )
    h2.SetLineColor( kBlue )   # if no fill color is defined, the line color is used to fill stacked histograms
    
    h3 = TH1D( 'hTest3', 'Data', 20, -4, 4 )
    h3.FillRandom( 'gaus', 1500 )
    h3.SetLineColor( kBlack )
    h3.SetMarkerSize( 1.0 )
    
    v1 = Variable('pt_tau', 'pt_tau', 'p_{T}(#tau)', 'GeV', Binning(20, -4, 4) )
    v2 = Variable('pt_jet', 'pt_jet', 'p_{T}(jet)', 'GeV', Binning(20, -4, 4) )
    #var_Entries.binning.high = 500
    p = PlotMatrix( 'TestMulti', (v1, v2), (var_Entries, var_Entries, var_Entries), (2,1), (2,2,1) )
    p.sizeY = 800
    p.sizeX = 600
    p.referenceSizeX = 600
    p.referenceSizeY = 600
    p.plots[0][0].addHistogram( h1, copy=True )
    p.plots[0][1].addHistogram( h2, copy=True )
    p.plots[1][0].addHistogram( h3, 'E1', copy=True )
    p.plots[1][1].addHistogram( h1, stacked=True )
    p.plots[1][1].addHistogram( h2, stacked=True )
    p.plots[1][1].addHistogram( h3, 'E0' )
    p.plots[0][2].addHistogram( h2, copy=True )
    p.plots[1][2].addHistogram( h1, copy=True )
    p.showBinWidthY = False
    p.draw()
    p.saveAs( 'testPlotMatrix_0.pdf' )
    p.draw()
    p.saveAs( 'testPlotMatrix_1.pdf' )
    
    raw_input( 'Continue?' )

def testDoublePlot():
    from ROOT import TH1D, TF1, kBlack, kRed, kBlue, kGreen
    from happy.style import mcErrorStyle
    from happy.variable import Variable
    from happy.plotDecorator import AtlasTitleDecorator
    
    h1 = TH1D( 'hTest1', 'First Test', 20, -4, 4 )
    h1.FillRandom( 'gaus', 1000 )
    h1.SetLineColor( kRed )
    
    h2 = TH1D( 'hTest2', 'Second Test', 20, -4, 4 )
    h2.FillRandom( 'gaus', 500 )
    h2.SetLineColor( kBlue )   # if no fill color is defined, the line color is used to fill stacked histograms
    
    h3 = TH1D( 'hTest3', 'Data', 20, -4, 4 )
    h3.FillRandom( 'gaus', 1500 )
    h3.SetLineColor( kBlack )
    
    hSum = TH1D( 'hSum', 'Stat', 20, -4, 4 )
    mcErrorStyle.applyTo( hSum )
    
    hRatio = TH1D( 'hRatio', 'Ratio', 20, -4, 4 )
    hRatio.Sumw2()
    
    xVar = Variable( 'Invariant Mass', unit='GeV', binning=Binning( 20, -4, 4 ) )
    yVarDown = Variable ( 'Ratio', binning=Binning(low=0.5, high=1.5) )
    yVarDown.centerTitle = True
    
    # Add the ATLAS label to each plot
    Canvas.defaultTitleDecorator = AtlasTitleDecorator( 'Internal' )
    
    p = Plot( 'testPlot', xVar )
    p.addHistogram( h3, 'E0' )
    p.addHistogram( h1, stacked=True )
    p.addHistogram( h2, stacked=True, copy=True )
    p.draw()
    
    # create a double plot with ratios by hand
    doubleTest = DoublePlot( 'testDoublePlot', xVar, var_Entries, yVarDown, 474, 192 )
    doubleTest.sizeY = 800
    doubleTest.referenceSizeY = 600
    doubleTest.plotUp.addHistogram( hSum, 'E2' )
    doubleTest.plotUp.addHistogram( h3, 'E0' )
    hSum.Add( doubleTest.plotUp.addHistogram( h1, stacked=True ) )
    hSum.Add( doubleTest.plotUp.addHistogram( h2, stacked=True, copy=True ) )
    hRatio.Add( h3 )
    hRatio.Divide( hSum )
    doubleTest.plotDown.addHistogram( hRatio, 'E0' )
    doubleTest.draw()
    doubleTest.saveAs( 'testDoublePlot_0.pdf' )
    doubleTest.logY = True
    doubleTest.draw()
    doubleTest.saveAs( 'testDoublePlot_1.pdf' )
    
    raw_input( 'Continue?' )
                
if __name__ == '__main__':
    testMultiPlot()
    testDoublePlot()
    pass
    
