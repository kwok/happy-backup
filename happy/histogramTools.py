## @package happy.histogramTools
#  @brief Helper methods to manipulate histograms and graphs.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.style import Style
from happy.miscellaneous import calculatePoissonErrors
from happy.stringTools import appendUUID
import os, math, logging

logger = logging.getLogger( __name__ )

## Copies all attributes from one ROOT object to another
#  Supports TH1, TGraph and all related classes.
#  @param source   histogram from which the style is taken
#  @param target   histogram to which the style is applied
def copyStyle( source, target ):
    target.SetMarkerStyle( source.GetMarkerStyle() )
    target.SetMarkerColor( source.GetMarkerColor() )
    target.SetMarkerSize( source.GetMarkerSize() )
    target.SetLineStyle( source.GetLineStyle() )
    target.SetLineColor( source.GetLineColor() )
    target.SetLineWidth( source.GetLineWidth() )
    target.SetFillColor( source.GetFillColor() )
    target.SetFillStyle( source.GetFillStyle() )

## Removes all entries from histogram bins between low and high.
#  Also removes entries from bins that fall only partially into the blinded region.
#  @attention: This method modifies the passed histogram.
#  @param histogram    TH1 object to be blinded
#  @param low          lower limit of the blinded region
#  @param high         upper limit of the blinded region
#  @return blinded histogram
def blindHistogram( histogram, low, high ):
    xAxis = histogram.GetXaxis()
    for iBin in range( 1, histogram.GetNbinsX()+1 ):
        if xAxis.GetBinUpEdge( iBin ) > low and xAxis.GetBinLowEdge( iBin ) < high:
            histogram.SetBinContent( iBin, 0 )
            histogram.SetBinError( iBin, 1e-9 )
    return histogram

## Removes all entries from a 2D histogram within the given limits in x and y.
#  Also removes entries from bins that fall only partially into the blinded region.
#  @attention: This method modifies the passed histogram.
#  @param histogram    TH1 object to be blinded
#  @param lowX         lower limit of the blinded region along the x axis
#  @param highX        upper limit of the blinded region along the x axis
#  @param lowY         lower limit of the blinded region along the y axis
#  @param highY        upper limit of the blinded region along the y axis
#  @return blinded histogram
def blindHistogram2D( histogram, lowX, highX, lowY, highY ):
    xAxis = histogram.GetXaxis()
    yAxis = histogram.GetYaxis()
    for iBinX in range( 1, xAxis.GetNbins()+1 ):
        if xAxis.GetBinUpEdge( iBinX ) < lowX:
            continue
        if xAxis.GetBinLowEdge( iBinX ) > highX:
            break
        for iBinY in range( 1, yAxis.GetNbins()+1 ):
            histogram.SetBinContent( iBinX, iBinY, 0. )
            histogram.SetBinError( iBinX, iBinY, 1e-9 )
    for iBinY in range( 1, yAxis.GetNbins()+1 ):
        if yAxis.GetBinUpEdge( iBinY ) < lowY:
            continue
        if yAxis.GetBinLowEdge( iBinY ) > highY:
            break
        for iBinX in range( 1, xAxis.GetNbins()+1 ):
            histogram.SetBinContent( iBinX, iBinY, 0. )
            histogram.SetBinError( iBinX, iBinY, 1e-9 )
    return histogram

## Helper method to convert a histogram to a corresponding graph.
#  A point is added for each histogram bin with an x value corresponding to the bin centre and
#  a y value corresponding to the bin content. Under- and overflow bins are ignored.
#  @param hist           TH1 object
#  @param name           name of the graph (optional, default is name of histogram)
#  @param keepErrors     decide if the y-errors should be propagated to the graph
#  @param poissonErrors  decide if the y-errors should be calculated as Poisson errors
#  @param keepXErrors    decide if the bin-width should be kept as errors in x direction
#  @return TGraphAsymmErrors
def histToGraph( hist, name='', keepErrors=True, poissonErrors=True, keepXErrors=True ):
    if not name:
        name = 'g%s' % ( hist.GetName() )
    from ROOT import TGraphAsymmErrors, TGraph
    if isinstance(hist, TGraph):
        return hist.Clone(name)
    nBins = hist.GetNbinsX()
    graph = TGraphAsymmErrors( nBins )
    graph.SetNameTitle( name, hist.GetTitle() )
    xAxis = hist.GetXaxis()
    for i in xrange( nBins ):
        xVal = xAxis.GetBinCenter( i+1 )
        yVal = hist.GetBinContent( i+1 )
        graph.SetPoint( i, xVal, yVal )
        if keepXErrors:
            graph.SetPointEXlow( i, abs(xVal - xAxis.GetBinLowEdge( i+1 )) )
            graph.SetPointEXhigh( i, abs(xVal - xAxis.GetBinUpEdge( i+1 )) )
        if keepErrors:
            if poissonErrors:
                lo, hi = calculatePoissonErrors( yVal )
                graph.SetPointEYlow( i, lo )
                graph.SetPointEYhigh( i, hi )
            else:
                graph.SetPointEYlow( i, hist.GetBinErrorLow( i+1 ) )
                graph.SetPointEYhigh( i, hist.GetBinErrorUp( i+1 ) )
    copyStyle( hist, graph )
    return graph

## Helper method to create two outline histograms from a graph.
#  The histogram bin contents correspond to the upper and lower boundaries of
#  the errors of the graph. 
#  @param hist           TH1 object
#  @param name           name of the graph (optional, default is name of histogram)
#  @param keepErrors     decide if the y-errors should be propagated to the graph
#  @param poissonErrors  decide if the y-errors should be calculated as Poisson errors
#  @param keepXErrors    decide if the bin-width should be kept as errors in x direction
#  @return TGraphAsymmErrors
def graphToOutlineHist( hist, name='', keepErrors=True, poissonErrors=True, keepXErrors=True ):
    if not name:
        name = 'g%s' % ( hist.GetName() )
    from ROOT import TGraphAsymmErrors
    nBins = hist.GetNbinsX()
    graph = TGraphAsymmErrors( nBins )
    graph.SetNameTitle( name, hist.GetTitle() )
    xAxis = hist.GetXaxis()
    for i in xrange( nBins ):
        xVal = xAxis.GetBinCenter( i+1 )
        yVal = hist.GetBinContent( i+1 )
        graph.SetPoint( i, xVal, yVal )
        if keepXErrors:
            graph.SetPointEXlow( i, abs(xVal - xAxis.GetBinLowEdge( i+1 )) )
            graph.SetPointEXhigh( i, abs(xVal - xAxis.GetBinUpEdge( i+1 )) )
        if keepErrors:
            if poissonErrors:
                lo, hi = calculatePoissonErrors( yVal )
                graph.SetPointEYlow( i, lo )
                graph.SetPointEYhigh( i, hi )
            else:
                graph.SetPointEYlow( i, hist.GetBinErrorLow( i+1 ) )
                graph.SetPointEYhigh( i, hist.GetBinErrorUp( i+1 ) )
    copyStyle( hist, graph )
    return graph

## Helper method to create a new histogram with different binning.
#  The original content is filled into the new histogram accordingly.
#  @param histogram    TH1 or related that should be rebinned
#  @param variable     Variable that defines the new histogram
#  @return new TH1 with updated binning
def rebinIntoNewHist( histogram, variable ):
    newHist = variable.createHistogram( histogram.GetTitle() )
    for oldBin in xrange( histogram.GetNbinsX() + 2 ):
        newBin = newHist.FindBin( histogram.GetBinCenter( oldBin ) )
        newHist.SetBinContent( newBin, newHist.GetBinContent( newBin ) + histogram.GetBinContent( oldBin ) )
        newHist.SetBinError( newBin, math.sqrt( histogram.GetBinError( newBin )**2 + histogram.GetBinError( oldBin )**2 ) )
    newHist.SetEntries( histogram.GetEntries() )
    copyStyle( histogram, newHist )
    return newHist

## Helper method to combine statistical and systematic uncertainties into a single TGraph.
#  Number of points of the graph has to match the number of bins of the histogram.
#  By default the graph is modified, unless copy is enabled where only the copy is
#  modified and returned. Uncertainties are added in quadrature.
#  @param hist        TH1 containing the statistical uncertainties
#  @param graph       TGraphAsymmErrors containing the systematic uncertainties
#  @param copy        decide if the graph should be copied instead of modified
#  @return TGraphAsymmErrors with the combined uncertainties
def combineStatsAndSystematics( hist, graph, copy=False ):
    nBins = hist.GetNbinsX()
    if graph.GetN() != nBins:
        logger.error( 'combineStatsAndSystematics(): number of points of graphs %s does not match number of bins of histogram %s' % (graph, hist) )
        return graph
    if copy:
        # generate a copy with a unique name
        graph = graph.Clone( appendUUID( graph.GetName() ) )
    xAxis = hist.GetXaxis()
    for i in xrange( nBins ):
        xVal = xAxis.GetBinCenter( i+1 )
        graph.SetPoint( i, xVal, hist.GetBinContent( i+1 ) )
        graph.SetPointEYlow( i, ( hist.GetBinErrorLow( i+1 )**2 + graph.GetErrorYlow( i )**2 )**0.5 )
        graph.SetPointEYhigh( i, ( hist.GetBinErrorUp( i+1 )**2 + graph.GetErrorYhigh( i )**2 )**0.5 )
    return graph

## Helper method to calculate S / sqrt(S+B) from two input graphs.
#  Replaces each points y values with y1 / (scaleFactor*y2). Number of points of
#  both graphs has to match. Also supports 1D histograms. In that case the number
#  bins has to match the number of points. Errors in y-direction are propagated
#  assuming that uncertainties are uncorrelated. In case they are correlated use
#  ignoreErrorsY = True, which will result in yError1 = yError1 / y2 for each point.
#  By default the first graph is modified. If copy is enabled only the copy is
#  modified and returned.
#  @param signalHist     TH1 object
#  @param bkgHist        TGraph or TH1 object
#  @param copy           decide if the first histogram should be copied instead of modified (optional, default is False)
#  @param ignoreErrorsY  do not propagate y errors of otherGraph (optional, default is False)
#  @return calculated TGraphAsymmErrors
def SoverSqrtSplusBHistogram( signalHist, bkgHist, copy=False, ignoreErrorsY=False ):
    from ROOT import TH1, TGraph
    if isinstance( bkgHist, TGraph ) and signalHist.GetNbinsX() != bkgHist.GetN():
        logger.error( 'SoverSqrtSplusBGraph(): number of bins of histograms %s does not match number of points of graph %s' % (signalHist, bkgHist) )
        return signalHist
    elif isinstance( bkgHist, TH1 ) and signalHist.GetNbinsX() != bkgHist.GetNbinsX():
        logger.error( 'SoverSqrtSplusBGraph(): number of bins of histograms %s and %s does not match' % (signalHist, bkgHist) )
        return signalHist
    if copy:
        # generate a copy with a unique name
        signalHist = signalHist.Clone( appendUUID( signalHist.GetName() ) )
    from ROOT import Double
    x, y1, y2 = (Double(), Double(), Double())
    for i in xrange(1, signalHist.GetNbinsX()+1):
        if isinstance( bkgHist, TH1 ):
            y2 = bkgHist.GetBinContent( i )
            y2hi = bkgHist.GetBinErrorUp( i )
        else:
            bkgHist.GetPoint( i, x, y2 )
            y2hi = bkgHist.GetErrorYhigh( i-1 )
        y1 = signalHist.GetBinContent( i )
        if ignoreErrorsY:
            y2hi = 0.
        if y1+y2 == 0:
            # dividing by 0 is evil
            signalHist.SetBinContent( i, 0. )
            signalHist.SetBinError( i, 0. )
        else:
            # x value remains unchanged
            signalHist.SetBinContent( i, y1 / (y1+y2)**0.5 )
            # no need to treat the errors in x
            signalHist.SetBinError( i, ((signalHist.GetBinError( i )*(0.5*(y1+2*y2)/(y1+y2)**1.5))**2 + (y2hi*(0.5*(y1)/(y1+y2)**1.5))**2)**0.5 )
    return signalHist

## Helper method to add two TGraph objects together (similar TH1::Add).
#  Number of points of both graphs has to match. Errors in y-direction are added
#  in quadrature. By default the first graph is modified. If copy is enabled
#  only the copy is modified and returned.
#  @param graph          TGraphAsymmErrors object
#  @param otherGraph     TGraph object
#  @param scaleFactor    factor that is multiplied to the values of the other graph (optional, default is 1)
#  @param copy           decide if the graph should be copied instead or modified
#  @return combined TGraphAsymmErrors
def addGraphs( graph, otherGraph, scaleFactor=1.0, copy=False ):
    from ROOT import TH1, TGraph
    if isinstance( otherGraph, TH1 ) and graph.GetN() != otherGraph.GetNbinsX():
        logger.error( 'addGraphs(): number of points of graphs %s does not match number of bins of histogram %s' % (graph, otherGraph) )
        return graph
    elif isinstance( otherGraph, TGraph ) and graph.GetN() != otherGraph.GetN():
        logger.error( 'addGraphs(): number of points of graphs %s and %s does not match' % (graph, otherGraph) )
        return graph
    if copy:
        # generate a copy with a unique name
        graph = graph.Clone( appendUUID( graph.GetName() ) )
    from ROOT import Double
    x, y1, y2 = (Double(), Double(), Double())
    for i in xrange(graph.GetN()):
        if isinstance( otherGraph, TH1 ):
            y2 = otherGraph.GetBinContent( i+1 )
            y2lo = otherGraph.GetBinErrorLow( i+1 )
            y2hi = otherGraph.GetBinErrorUp( i+1 )
        else:
            otherGraph.GetPoint( i, x, y2 )
            y2lo = otherGraph.GetErrorYlow( i )
            y2hi = otherGraph.GetErrorYhigh( i )
        graph.GetPoint( i, x, y1 )
        # x value remains unchanged
        graph.SetPoint( i, x, y1+scaleFactor*y2 )
        # no need to treat the errors in x
        graph.SetPointEYlow( i, ( graph.GetErrorYlow( i )**2 + (scaleFactor*y2lo)**2 )**0.5 )
        graph.SetPointEYhigh( i, ( graph.GetErrorYhigh( i )**2 + (scaleFactor*y2hi)**2 )**0.5 )
    return graph

## Helper method to divide one graph by another graph (similiar to TH1::Divide).
#  Replaces each points y values with y1 / y2. Number of points of both graphs
#  has to match. Also supports 1D histograms. In that case the number bins has to
#  match the number of points. Errors in y-direction are propagated assuming that
#  uncertainties are uncorrelated. In case they are correlated use
#  ignoreErrorsY = True, which will result in yError1 = yError1 / y2 for each point.
#  By default the first graph is modified. If copy is enabled only the copy is
#  modified and returned.
#  @param graph          TGraphAsymmErrors object
#  @param otherGraph     TGraph or TH1 object
#  @param copy           decide if the first graph should be copied instead of modified (optional, default is False)
#  @param ignoreErrorsY  do not propagate y errors of otherGraph (optional, default is False)
#  @return divided TGraphAsymmErrors
def divideGraphs( graph, otherGraph, copy=False, ignoreErrorsY=False ):
    from ROOT import TH1, TGraph
    if isinstance( otherGraph, TH1 ) and graph.GetN() != otherGraph.GetNbinsX():
        logger.error( 'divideGraphs(): number of points of graphs %s does not match number of bins of histogram %s' % (graph, otherGraph) )
        return graph
    elif isinstance( otherGraph, TGraph ) and graph.GetN() != otherGraph.GetN():
        logger.error( 'divideGraphs(): number of points of graphs %s and %s does not match' % (graph, otherGraph) )
        return graph
    if copy:
        # generate a copy with a unique name
        graph = graph.Clone( appendUUID( graph.GetName() ) )
    from ROOT import Double
    x, y1, y2 = (Double(), Double(), Double())
    for i in xrange(graph.GetN()):
        if isinstance( otherGraph, TH1 ):
            y2 = otherGraph.GetBinContent( i+1 )
            y2lo = otherGraph.GetBinErrorLow( i+1 )
            y2hi = otherGraph.GetBinErrorUp( i+1 )
        else:
            otherGraph.GetPoint( i, x, y2 )
            y2lo = otherGraph.GetErrorYlow( i )
            y2hi = otherGraph.GetErrorYhigh( i )
        graph.GetPoint( i, x, y1 )
        if ignoreErrorsY:
            y2lo = 0.
            y2hi = 0.
        if y2 == 0:
            # dividing by 0 is evil
            graph.SetPoint( i, x, 0. )
            graph.SetPointEYlow( i, 0. )
            graph.SetPointEYhigh( i, 0. )
        else:
            # x value remains unchanged
            graph.SetPoint( i, x, y1 / y2 )
            # no need to treat the errors in x
            graph.SetPointEYlow( i, ((graph.GetErrorYlow( i )*y2)**2 + (y2lo*y1)**2)**0.5 / y2**2 )
            graph.SetPointEYhigh( i, ((graph.GetErrorYhigh( i )*y2)**2 + (y2hi*y1)**2)**0.5 / y2**2 )
    return graph

## Helper method to normalise a TGraph to the entries in each bin of a reference histogram.
#  Divides the y-value and the y-uncertainty by the ratio of the y-value and the bin
#  content of the reference histogram for each bin. The number of bins and the number of points
#  of the graph have to match.
#  @param graph        TGraphAsymmErrors object
#  @param histogram    TH1 object
#  @param copy         decide if the graph should be copied instead of modified (optional, default is False)
#  @return TGraph
def normaliseGraphToBinContent( graph, histogram, copy=False ):
    if graph.GetN() != histogram.GetNbinsX():
        logger.error( 'normaliseGraphToBinContent(): number of points of graph %s does not match number of bins of histogram %s' % (graph, histogram) )
        return graph
    if copy:
        # generate a copy with a unique name
        graph = graph.Clone( appendUUID( graph.GetName() ) )
    from ROOT import Double
    x, y1, y2 = (Double(), Double(), Double())
    for i in xrange(graph.GetN()):
        y2 = histogram.GetBinContent( i+1 )
        graph.GetPoint( i, x, y1 )
        # x value remains unchanged
        graph.SetPoint( i, x, y2 )
        # no need to treat the errors in x
        if y1 != 0.:
            graph.SetPointEYlow( i, graph.GetErrorYlow( i ) * y2 / y1 )
            graph.SetPointEYhigh( i, graph.GetErrorYhigh( i ) * y2 / y1 )
    return graph

## Helper method to calculate S / sqrt(S+B) from two input graphs.
#  Replaces each points y values with y1 / (scaleFactor*y2). Number of points of
#  both graphs has to match. Also supports 1D histograms. In that case the number
#  bins has to match the number of points. Errors in y-direction are propagated
#  assuming that uncertainties are uncorrelated. In case they are correlated use
#  ignoreErrorsY = True, which will result in yError1 = yError1 / y2 for each point.
#  By default the first graph is modified. If copy is enabled only the copy is
#  modified and returned.
#  @param signalGraph    TGraphAsymmErrors object
#  @param bkgGraph       TGraph or TH1 object
#  @param copy           decide if the first graph should be copied instead of modified (optional, default is False)
#  @param ignoreErrorsY  do not propagate y errors of otherGraph (optional, default is False)
#  @return calculated TGraphAsymmErrors
def SoverSqrtSplusBGraph( signalGraph, bkgGraph, copy=False, ignoreErrorsY=False ):
    from ROOT import TH1, TGraph
    if isinstance( bkgGraph, TH1 ) and signalGraph.GetN() != bkgGraph.GetNbinsX():
        logger.error( 'SoverSqrtSplusBGraph(): number of points of graphs %s does not match number of bins of histogram %s' % (signalGraph, bkgGraph) )
        return signalGraph
    elif isinstance( bkgGraph, TGraph ) and signalGraph.GetN() != bkgGraph.GetN():
        logger.error( 'SoverSqrtSplusBGraph(): number of points of graphs %s and %s does not match' % (signalGraph, bkgGraph) )
        return signalGraph
    if copy:
        # generate a copy with a unique name
        signalGraph = signalGraph.Clone( appendUUID( signalGraph.GetName() ) )
    from ROOT import Double
    x, y1, y2 = (Double(), Double(), Double())
    for i in xrange(signalGraph.GetN()):
        if isinstance( bkgGraph, TH1 ):
            y2 = bkgGraph.GetBinContent( i+1 )
            y2lo = bkgGraph.GetBinErrorLow( i+1 )
            y2hi = bkgGraph.GetBinErrorUp( i+1 )
        else:
            bkgGraph.GetPoint( i, x, y2 )
            y2lo = bkgGraph.GetErrorYlow( i )
            y2hi = bkgGraph.GetErrorYhigh( i )
        signalGraph.GetPoint( i, x, y1 )
        if ignoreErrorsY:
            y2lo = 0.
            y2hi = 0.
        if y1+y2 == 0:
            # dividing by 0 is evil
            signalGraph.SetPoint( i, x, 0. )
            signalGraph.SetPointEYlow( i, 0. )
            signalGraph.SetPointEYhigh( i, 0. )
        else:
            # x value remains unchanged
            signalGraph.SetPoint( i, x, y1 / (y1+y2)**0.5 )
            # no need to treat the errors in x
            signalGraph.SetPointEYlow( i, ((signalGraph.GetErrorYlow( i )*(0.5*(y1+2*y2)/(y1+y2)**1.5))**2 + (y2lo*(0.5*(y1)/(y1+y2)**1.5))**2)**0.5 )
            signalGraph.SetPointEYhigh( i, ((signalGraph.GetErrorYhigh( i )*(0.5*(y1+2*y2)/(y1+y2)**1.5))**2 + (y2hi*(0.5*(y1)/(y1+y2)**1.5))**2)**0.5 )
    return signalGraph

## Resets all values and errors of a given TGraphAsymmErrors to 0.
#  @param graph   TGraphAsymmErrors object
#  @return modified TGraphAsymmErrors
def resetGraph( graph ):
    from ROOT import Double
    x, y = (Double(), Double())
    for i in xrange(graph.GetN()):
        graph.GetPoint( i, x, y )
        graph.SetPoint( i, x, 0. )
        graph.SetPointEYlow( i, 0. )
        graph.SetPointEYhigh( i, 0. )
    return graph

## Replace the uncertainties of the given TGraphAsymmErrors with Poisson errors.
#  The original errors in y will be ignored and replaced with Poisson errors based on
#  the y value for each point of the graph.
#  @param graph    TGraphAsymmErrors object
#  @return modified TGraphAsymmErrors
def convertGraphToPoissonErrors( graph ):
    from ROOT import Double
    x, y = (Double(), Double())
    for i in xrange(graph.GetN()):
        graph.GetPoint( i, x, y )
        # no need to treat the errors in x
        lo, hi = calculatePoissonErrors( y )
        graph.SetPointEYlow( i, lo )
        graph.SetPointEYhigh( i, hi )
    return graph

## Helper method to divide one histogram by another histogram (similiar to TH1::Divide but not identical).
#  Number of bins of both histograms has to match. Errors in y-direction are divided by the central value.
#  The errors of the second histogram are not propagated. By default the first histogram is modified.
#  If copy is enabled only the copy is modified and returned.
#  @param histogram       TH1 object
#  @param otherHistogram  TH1 object
#  @param copy            decide if the first histogram should be copied instead of modified
#  @return combined histogram
def divideHistograms( histogram, otherHistogram, copy=False ):
    if histogram.GetNbinsX() != otherHistogram.GetNbinsX():
        logger.error( 'divideHistograms(): number of bins of histograms %s and %s do not match' % (histogram, otherHistogram) )
        return histogram
    if copy:
        # generate a copy with a unique name
        histogram = histogram.Clone( appendUUID( histogram.GetName() ) )
    for i in xrange( 0, histogram.GetNbinsX()+1):
        y = otherHistogram.GetBinContent( i )
        if y == 0.:
            histogram.SetBinContent( i, 0. )
            histogram.SetBinError( i, 0. )
        else:
            histogram.SetBinContent( i, histogram.GetBinContent( i ) / y )
            histogram.SetBinError( i, histogram.GetBinError( i ) / y )
    return histogram 

## Helper method to fill the content of the under- and overflow bins into the first and last bin respectively.
#  Updates bin content and bin errors accordingly. The overflow and underflow bins will be empty afterwards.
#  @param hist   TH1 object
#  @param low    add the underflow bin into the first bin (optional, default is True)
#  @param high   add the overflow bin into the last bin (optional, default is True)
#  @param copy   decide if the histogram should be copied instead of modified (optional)
#  @return histogram with overflow entries inside the histogram range
def overflowIntoLastBins( hist, low=True, high=True, copy=False ):
    if copy:
        # generate a copy with a unique name
        hist = hist.Clone( appendUUID( hist.GetName() ) )
    nBins = hist.GetNbinsX()
    bins = []
    if low:
        bins.append( (0,1) )
    if high:
        bins.append( (nBins+1, nBins) )
    for fromBin, toBin in bins:
        hist.SetBinContent( toBin, hist.GetBinContent(fromBin) + hist.GetBinContent(toBin) )
        hist.SetBinError( toBin, math.sqrt(hist.GetBinError(fromBin)**2 + hist.GetBinError(toBin)**2) )
        hist.SetBinContent( fromBin, 0. )
        hist.SetBinError( fromBin, 0. )
    return hist

## Creates a list of TArrow objects to indicate points outside of the given boundaries.
#  @param obj          TGraph or TH1 object
#  @param minimum      minimum to be considered
#  @param maximum      maximum to be considered
#  @param arrowSize    size of the arrow tip in units of the pad size
#  @param arrowLength  length of the arrow in units of (maximum - minimum)
#  @param arrowOption  style of the arrow tip (see TArrow documentation)
#  @param style        Style object defining the arrow color
#  @return list of TArrow objects
def createOutOfRangeArrows( obj, minimum, maximum, arrowSize=0.01, arrowLength=0.1, arrowOption='|>', style=Style( 2, lineWidth=2, fillStyle=1001 ) ):
    from ROOT import TArrow, TGraph, TH1, Double
    arrows = []
    arrowLength = arrowLength * ( maximum - minimum ) 
    if isinstance( obj, TH1):
        for iBin in xrange( 1, obj.GetNbinsX()+1 ):
            x = obj.GetBinCenter( iBin )
            y = obj.GetBinContent( iBin )
            if minimum > y:
                arrows.append( TArrow( x, minimum + 1.1*arrowLength, x, minimum + 0.1*arrowLength, arrowSize, arrowOption ) )
            elif maximum < y:
                arrows.append( TArrow( x, maximum - 1.1*arrowLength, x, maximum - 0.1*arrowLength, arrowSize, arrowOption ) )
    elif isinstance( obj, TGraph ):
        x, y = ( Double(), Double() )
        for iBin in xrange( 0, obj.GetN() ):
            obj.GetPoint( iBin, x, y )
            if minimum > y:
                arrows.append( TArrow( x, minimum + 1.1*arrowLength, x, minimum + 0.1*arrowLength, arrowSize, arrowOption ) )
            elif maximum < y:
                arrows.append( TArrow( x, maximum - 1.1*arrowLength, x, maximum - 0.1*arrowLength, arrowSize, arrowOption ) )
    if style:
        for arrow in arrows:
            style.applyTo( arrow )
    return arrows

## Method to collect all histogram type objects from a TDirectory, ie. a ROOT file
#  @param directory     TDirectory object which is scanned recursively
#  @return dictionary that maps path to histogram object
def extractHistogramsFromTDirectory( directory ):
    from ROOT import gROOT, TDirectory, TH1
    histograms = {}
    directoryPath = directory.GetPath().split(':')[1]
    for key in directory.GetListOfKeys():
        className = key.GetClassName()
        classObj = gROOT.GetClass( className )
        keyName = key.GetName()
        if classObj.InheritsFrom( TDirectory.Class() ):
            histograms.update( extractHistogramsFromTDirectory( directory.Get( keyName ) ) )
        elif classObj.InheritsFrom( TH1.Class() ):
            histograms[ os.path.join( directoryPath, keyName ) ] = directory.Get( keyName )
        else:
            pass
    return histograms
