## @package happy.scheduler
#  @brief Module that handles all histogram requests from Dataset objects.
#  For Datasets with a HistogramStore it can delay the creation of histograms
#  and process them in a single loop. The loop is grouped by Dataset and input
#  TTree and thus the most efficienct way to fill many histograms.
#
#  Usage of the scheduler:
#  -# activate the scheduling:
#     @code
# import HAPPy.core.scheduler
# scheduler.isActive = True
#     @endcode
#  -# registering the histogram queries:
#     This happens automatically when getHistogram() or similar methods get called on a Dataset.
#     Make sure all your datasets have a HistogramStore defined first!
#     Simply run your usual code to process all histograms to make sure all queries are registered.
#     Histograms are not actually filled while scheduler is active!
#  -# filling the histograms and storing them in the respective HistogramStore:
#     @code
# scheduler.process()
#     @endcode
#  -# using the histograms:
#     Run your code to process all histograms a second time.
#     Now the filled histograms are available in the HistogramStore and can be retrieved
#
# @remark If the program logic requires a histogram to be created immediately,
# the scheduler must be suspended for that part of the code.
# @code
# schedulerState = scheduler.isActive
# scheduler.isActive = False
# # create some histograms ...
# scheduler.isActive = schedulerState
# @endcode
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.cut import Cut
from happy.parallel import FuncWorker, run_pool
import os, logging
import multiprocessing
import gconfig

# import sys
# sys.path.append("/afs/cern.ch/work/k/kchu/lfv_lh/symm_lephad/boom/boom/")
import imp
foo = imp.load_source('lumi', "/afs/cern.ch/work/k/kchu/lfv_lh/symm_lephad/boom/boom/lumi.py")


## Logger
logger = logging.getLogger( __name__ )

## Boolean to steer if histogram scheduling is active
isActive = False

## Maximum entries to process per tree, for debugging
maxEntries = -1

## Internal dictionary to store queries for later use
QUERYDICT = {}

## Helper class to encapsulate all the information to create a histogram at a later point.
class HistogramQuery( object ):
    
    ## Default constructor.
    #  @param histogram             TH1, TH2, TProfile or derived object
    #  @param systematicVariation   SystematicVariation object
    #  @param xVariable             Variable object used for the x-axis
    #  @param yVariable             Variable object used for the y-axis (optional)
    #  @param cut                   Cut object defining the selection (optional)
    #  @param weightExpression      Cut object defining the weight expression (optional)
    def __init__( self, histogram, systematicVariation, xVariable, yVariable=None, cut=Cut(), weightExpression=Cut() ):
        self.histogram = histogram
        self.systematicVariation = systematicVariation
        self.xVariable = xVariable
        self.yVariable = yVariable
        self.cut = cut
        self.weightExpression = weightExpression
    
    def __repr__( self ):
        return 'HistogramQuery( %r, %r, %r, %r, %r, %r )' % ( self.histogram, self.systematicVariation, self.xVariable, self.yVariable, self.cut, self.weightExpression )
    
    ## Add this query to a fill histogram loop.
    #  @param multiHistogramFill  MultiHistogramFill object
    def addTo( self, multiHistogramFill ):
        weightExpression = (self.cut * self.weightExpression).cut
        from ROOT import TH1, TH2, TProfile
        if isinstance( self.histogram, TProfile ):
            multiHistogramFill.add( self.histogram, self.xVariable.command, self.yVariable.command, weightExpression )
        elif isinstance( self.histogram, TH2 ):
            # print "isinstance( self.histogram, TH2 )"
            multiHistogramFill.add( self.histogram, self.xVariable.command, self.yVariable.command, weightExpression )
        elif isinstance( self.histogram, TH1 ):
            # print "isinstance( self.histogram, TH1 )"
            multiHistogramFill.add( self.histogram, self.xVariable.command, weightExpression )

## Internal method to add a query.
#  @param histogram            histogram the will be filled during processing
#  @param dataset              Dataset object containing definition of TTrees and HistogramStore
#  @param systematicVariation  SytematicVariation object defining the tree name and potential additional weight
#  @param xVariable            Variable object that defines the variable expression for x used in draw and the binning
#  @param yVariable            Variable object that defines the variable expression for y used in draw and the binning
#  @param cut                  Cut object that defines the applied cut
#  @param weightExpression     Cut object that defines the weight expression
def addQuery( histogram, dataset, systematicVariation, xVariable, yVariable, cut, weightExpression ):
    if not QUERYDICT.has_key( dataset ):
        QUERYDICT[ dataset ] = {}
    treeNameDict = QUERYDICT[ dataset ]
    treeName = systematicVariation.treeName
    if not treeNameDict.has_key( treeName ):
        treeNameDict[ treeName ] = []
    treeNameDict[ treeName ].append( HistogramQuery( histogram, systematicVariation, xVariable, yVariable, cut, weightExpression ) )
    logger.debug( 'addQuery(): added %r for tree "%s" in %r' % ( treeNameDict[treeName][-1], treeName, dataset ) )

## Produces a 1D histogram from a Dataset object.
#  If a HistogramStore is defined it will first try to find the histogram in the store.
#  If the scheduler is active the creation of the histogram will be delayed until the scheduler is processed
#  and an empty histogram is returned. Once processed, the histogram is then stored in the HistogramStore of the dataset
#  for later use. If the HistogramScheduler is not active the histogram is produced directly using TTree::Draw.
#  The "recreate" option allows to override existing histograms if they are already in the HistogramStore.
#  @param dataset              Dataset object containing definition of TTrees and HistogramStore
#  @param xVar                 Variable object that defines the variable expression for x used in draw and the binning
#  @param title                defines the histogram title
#  @param cut                  Cut object that defines the applied cut
#  @param weightExpression     Cut object that defines the weight expression
#  @param systematicVariation  SytematicVariation object defining the tree name and potential additional weights
#  @return TH1
def getHistogram( dataset, xVar, title, cut, weightExpression, systematicVariation ):
    hist = None
    print '$$$$$ you got the wrong getHistogram dumbass!'
    histogramStore = dataset.histogramStore
    
    print "!!!!!!!in getHistogram1D now!!!!!!"
    
    # first try to get the histogram from the histogram store
    if histogramStore:
        hist = histogramStore.getHistogram( dataset, systematicVariation, xVar, cut, weightExpression )
        if hist:
            hist.SetTitle( title )
    if not hist:
        # the histogram does not exist, yet. Need to produce it
        if isActive:
            # HistogramScheduler is not active, create and fill histogram now
            hist = xVar.createHistogram( title )
            if dataset.histogramStore:
                addQuery( hist, dataset, systematicVariation, xVar, None, cut, weightExpression )
            else:
                logger.error( 'getHistogram(): HistogramScheduler is active but %r has no HistogramStore defined. No place to store histograms for later use.' % dataset )
        else:
            # HistogramScheduler is not active, create and fill histogram now
            tree = dataset._open( systematicVariation.treeName )
            hist = xVar.createHistogramFromTree( tree, title, cut * weightExpression, maxEntries )
            if hist:
                # scale the histogram and store it in the HistogramStore
                if not dataset.isData and dataset.sumOfWeights:
                    hist.Scale( 1. / dataset.sumOfWeights )
                logger.debug( 'getHistogram: created histogram with yield %s' % hist.Integral() )
                if histogramStore:
                    histogramStore.putHistogram( dataset, systematicVariation, xVar, cut, weightExpression, hist )
    return hist

## Produces a 2D histogram from a Dataset object.
#  If a HistogramStore is defined it will first try to find the histogram in the store.
#  If the scheduler is active the creation of the histogram will be delayed until the scheduler is processed
#  and an empty histogram is returned. Once processed, the histogram is then stored in the HistogramStore of the dataset
#  for later use. If the HistogramScheduler is not active the histogram is produced directly using TTree::Draw.
#  The "recreate" option allows to override existing histograms if they are already in the HistogramStore.
#  @param dataset              Dataset object containing definition of TTrees and HistogramStore
#  @param xVar                 Variable object that defines the variable expression for x used in draw and the binning
#  @param yVar                 Variable object that defines the variable expression for y used in draw and the binning
#  @param title                defines the histogram title
#  @param cut                  Cut object that defines the applied cut
#  @param weightExpression     Cut object that defines the weight expression
#  @param systematicVariation  SytematicVariation object defining the tree name and potential additional weights
#  @param drawOption           draw option as used in TTree::Draw. Only used to extract if histogram should be a TProfile
#  @param recreate             force recreation of the histogram (don't read it from a possible histogram file)
#  @return TH2 or TProfile
def getHistogram2D( dataset, xVar, yVar, title, cut, weightExpression, systematicVariation, drawOption, recreate=False ):
    hist = None
    histogramStore = dataset.histogramStore
    isProfile = 'PROF' in drawOption.upper()
    
    print "!!!!!!!in getHistogram2D now!!!!!!"
    
    # first try to get the histogram from the histogram store
    if histogramStore and not recreate:
        hist = histogramStore.getHistogram2D( dataset, systematicVariation, xVar, yVar, cut, weightExpression )
        if hist:
            hist.SetTitle( title )
    if not hist:
        # the histogram does not exist, yet. Need to produce it
        if isActive:
            # scheduler is active. Create a dummy histogram and a query for when process() is called
            hist = xVar.create2DHistogram( yVar, title, isProfile )
            if dataset.histogramStore:
                addQuery( hist, dataset, systematicVariation, xVar, yVar, cut, weightExpression )
            else:
                logger.error( 'getHistogram2D(): HistogramScheduler is active but %r has no HistogramStore defined. No place to store histograms for later use.' % dataset )
        else:
            # HistogramScheduler is not active, create and fill histogram now
            tree = dataset._open( systematicVariation.treeName )
            hist = xVar.create2DHistogramFromTree( tree, yVar, title, cut * weightExpression, drawOption )
            if hist:
                # scale the histogram and store it in the HistogramStore
                if not isProfile and not dataset.isData and dataset.sumOfWeights:
                    hist.Scale( 1. / dataset.sumOfWeights )
                if histogramStore:
                    histogramStore.putHistogram2D( dataset, systematicVariation, xVar, yVar, cut, weightExpression, hist )
    return hist

## Main method to processes all queries.
#  The queries are grouped by datasets and trees so they can be processed in a single loop.
#  The histograms are stored in the HistogramStore of the respective Dataset for later use.
def process(n_cores=1):    
    # Compile the C++ code to fill multiple histograms in a single loop. Only compile once.
    try:
        from ROOT import MultiHistogramFill
    except ImportError:
        from ROOT import gROOT
        # gROOT.ProcessLine( '.L %s' % os.path.join( os.environ.get('HAPPyDIR'), 'ROOT', 'MultiHistogramFill.cxx+' ) )
        gROOT.ProcessLine( '.L %s' % os.path.join( os.environ.get('HAPPyDIR'), 'ROOT', 'MultiHistogramFill.cxx' ) )
        from ROOT import MultiHistogramFill
    #Gal: try to use cmake:
        
    # try:
    #     import mhfCmake
    # except ImportError:
    #     from ROOT import gROOT
    #     gROOT.ProcessLine( '.L %s' % os.path.join( '/afs/cern.ch/work/k/kchu/lfv_lh/symm_lephad/maindir/source/Runner/util', 'mhfCmake.cxx+' ) )
    #     import mhfCmake
    

    #gal:
    print '$$$$$$$$$$$$ gal: loaded MHF'
    # print QUERYDICT
    if n_cores != 1 and n_cores > multiprocessing.cpu_count() - 1:
        logger.warning('only {} cores available while {} requested, will use {} (keep one for access to the node)'.format(
                multiprocessing.cpu_count(), n_cores, multiprocessing.cpu_count() -1))
        n_cores = multiprocessing.cpu_count() -1

    if n_cores == 1:
        multiHistogramFill = MultiHistogramFill()
        for dataset, treeNameDict in QUERYDICT.items():  #gal: loop over enents?
            process_one_dataset(multiHistogramFill, dataset, treeNameDict)
            
    else:
        workers = []
        for dataset, treeNameDict in QUERYDICT.items():
            multiHistogramFill = MultiHistogramFill()
            workers.append(FuncWorker(
                    process_one_dataset, multiHistogramFill, dataset, treeNameDict))
        run_pool(workers, n_jobs=n_cores)
        for dataset in QUERYDICT.keys():
            dataset.histogramStore.close()

def process_one_dataset(filler, dataset, treeNameDict):
    #print '$$$$$$$$$$$ processin one dataset'
    counter=0
    # print treeNameDict
    for treeName, histogramQueries in treeNameDict.items():
        counter+=1
        tree = dataset._open( treeName )
        filler.setTree( tree )
        # print '$$$treeName:'
        # print treeName
        # print '$$$tree:'
        # print type(tree)
        # print '$$$dataset'
        # print dataset
        # print dataset.fileNames
        # add all queries from this TTree in this dataset to the loop
        for query in histogramQueries:
            # print "histogramQueries: query", query
            query.addTo( filler )
        # process the loop and fill all histograms

        #determine what MHF function to use:

        if gconfig.CreateSmallTree==1:
            tree_name = gconfig.SmallTreeFolder+ '/' +dataset.name +'_'+ str(counter)
            extra_weight=1.0
            # print tree_name
            if not dataset.isData and dataset.sumOfWeights:
                extra_weight = 1. / dataset.sumOfWeights 
            
            if not dataset.isData:
                sF = dataset.combinedScaleFactors #* dataset.systematicsSet.totalScaleFactor( None, sel.get_cut(dataset.isData) )
                for key,value in foo.LUMI_YEAR.items():
                    if key in dataset.name:
                        lum_year=value
                luminosity=foo.LUMI[lum_year] * foo.LUMI_SCALE[lum_year] 
                sF *= luminosity
                extra_weight*= sF
                # print "extra weight is: "+str(extra_weight)

            if not filler.create_tree(maxEntries,tree_name,extra_weight):
                # something went wrong, no histograms produced
                print "No Tree Created!"
                continue

        elif gconfig.UseRatio==0:
            print dataset.name
            if not filler.fill_sym(maxEntries,gconfig.anti_tau_ff):
            # if not filler.fill(maxEntries):
                # something went wrong, no histograms produced
                print "No Tree Created!"
                continue
        
        # elif gconfig.UseRatio==1 and gconfig.WhoSignal==1:
        elif gconfig.UseRatio==1:
            if( gconfig.WhoSignal==1 ):
                print dataset.name + '  mutau'
            elif( gconfig.WhoSignal==2 ):
                print dataset.name + '  etau'
            # extra_weight=1.0
            # if not dataset.isData and dataset.sumOfWeights:
            #     extra_weight = 1. / dataset.sumOfWeights 
            # if not dataset.isData:
            #     sF = dataset.combinedScaleFactors #* systematicsSet.totalScaleFactor( systematicVariation, cut )
            #     for key,value in foo.LUMI_YEAR.items():
            #         if key in dataset.name:
            #             lum_year=value
            #     luminosity=foo.LUMI[lum_year] * foo.LUMI_SCALE[lum_year] 
            #     sF *= luminosity
            #     extra_weight*= sF
            #     print "extra weight in scheduler is : "+str(extra_weight)

            if not filler.fill_mcRatio_mutau_signal(maxEntries,gconfig.anti_tau_ff,gconfig.WithEffCorr):
                # something went wrong, no histograms produced
                print "No Tree Created!"
                continue

        # elif gconfig.UseRatio==1 and gconfig.WhoSignal==2:
        #     print dataset.name
        #     if not filler.fill_mcRatio_eltau_signal(maxEntries,gconfig.anti_tau_ff):
        #         # something went wrong, no histograms produced
        #         print "No Tree Created!"
        #         continue

        else: print '$$$ Error, No MHF.fill function is set! check happy/scheduler.py $$$'



        # remove all queries from the histogram loop
        filler.clear()
        # go through the processed queries and store the histograms in the respective HistogramStore
        while histogramQueries:
            query = histogramQueries.pop()
            histogram = query.histogram
            histogramStore = dataset.histogramStore
            from ROOT import TH1, TH2, TProfile
            # scale the histogram and store it in the HistogramStore
            if isinstance( histogram, TProfile ):
                histogramStore.putHistogram2D( dataset, query.systematicVariation, query.xVariable, query.yVariable, query.cut, query.weightExpression, histogram )
            else:
                if not dataset.isData and dataset.sumOfWeights:
                    # print "histogram name:", histogram.name
                    print "histogram integral =", histogram.Integral()
                    print "histogram.Scale: 1. /", dataset.sumOfWeights
                    histogram.Scale( 1. / dataset.sumOfWeights )
                if isinstance( histogram, TH2 ):
                    histogramStore.putHistogram2D( dataset, query.systematicVariation, query.xVariable, query.yVariable, query.cut, query.weightExpression, histogram )
                elif isinstance( histogram, TH1 ):
                    histogramStore.putHistogram( dataset, query.systematicVariation, query.xVariable, query.cut, query.weightExpression, histogram )
    # close dataset store                   
    dataset.histogramStore.close()
