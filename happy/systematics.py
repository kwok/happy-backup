## @package happy.systematics
#  @brief Classes to handle systematic variations on Dataset objects
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.cut import Cut
from happy.miscellaneous import wrapMethod
import hashlib

## Class that represents a single systematic variation.
#  Generic base class from which all other types of systematic
#  variations are derived. Don't use directly.
class SystematicVariation( object ):

    ## Default constructor
    #  @param name                unique identifier (avoid spaces)
    #  @param title               title used for example in legends (use TLatex, optional, default uses name)
    def __init__( self, name, title='' ):
        ## name to identify the object
        self.name = name
        ## string used for example in legends
        self.title = title if title else self.name
        ## Systematics object that contains this systematic variation
        self.systematics = None
        
    def __hash__( self ):
        # only hashes the tree name
        return hash( self.name )
    
    def __eq__( self, other ):
        # compares only the tree name
        return isinstance( other, SystematicVariation ) and self.name == other.name
    
    ## Name of the tree used for this systematic variation
    @property
    def treeName( self ):
        return None
    
    ## Weight expression used for this systematic variation
    @property
    def weightExpression( self ):
        return ''
    
    ## Scale factor applied for this systematic variation
    @property
    def scale( self ):
        return 1.0

    ## Check if this is a variation of the tree name
    @property
    def isTreeSystematics( self ):
        return False
    
    ## Check if this is a variation of the weight expression
    @property
    def isWeightSystematics( self ):
        return False
    
    ## Check if this is a variation of the scale factor
    @property
    def isScaleSystematics( self ):
        return False
    
    ## Check if this is a variation of the histogram shape
    #  Either variation of the tree name or the weight expression
    @property
    def isShapeSystematics( self ):
        return self.isTreeSystematics or self.isWeightSystematics
    
    ## Check if this is a variation of the sum of weights
    @property
    def isSumOfWeightSystematics( self ):
        return False
    
    def __repr__( self ):
        return 'SystematicVariation(%r, %r)' % (self.name, self.title)

    
## Class that represents a variation of a global scale factor
class ScaleSystematicVariation( SystematicVariation ):
    ## Default constructor
    #  @param name                unique identifier (avoid spaces)
    #  @param title               title used for example in legends (use TLatex, optional, default uses name)
    #  @param scale               scale factor (optional, default is 1.0)
    def __init__( self, name, title='', scale=1.0 ):
        SystematicVariation.__init__( self, name, title )
        self.scale = scale
    
    def __hash__( self ):
        # only hashes the scale factor
        return hash( self.scale )
    
    def __eq__( self, other ):
        # compares only the scale factor
        return isinstance( other, ScaleSystematicVariation ) and self.scale == other.scale
    
    def __repr__( self ):
        return 'ScaleSystematicVariation(%r, %r, %r)' % (self.name, self.title, self.scale)
    
    ## Generates an MD5 hash for this object to identify this variation.
    #  Only hashes the scale factor itself, not the name or the title
    @property
    def md5( self ):
        return hashlib.md5( str( self.scale ) ).hexdigest()
    
    @property
    def scale( self ):
        return self.__scale
    
    @scale.setter
    def scale( self, value ):
        self.__scale = value
        
    @property
    def isScaleSystematics( self ):
        return True

## Class that represents a variation of the tree name
class TreeSystematicVariation( SystematicVariation ):
    ## Default constructor
    #  @param name                unique identifier (avoid spaces)
    #  @param title               title used for example in legends (use TLatex, optional, default uses name)
    #  @param treeName            tree name used 
    def __init__( self, name, title='', treeName=None ):
        SystematicVariation.__init__( self, name, title )
        self.treeName = treeName
    
    def __hash__( self ):
        # only hashes the tree name
        return hash( self.treeName )
    
    def __eq__( self, other ):
        # compares only the tree name
        return isinstance( other, TreeSystematicVariation ) and self.treeName == other.treeName
    
    def __repr__( self ):
        return 'TreeSystematicVariation(%r, %r, %r)' % (self.name, self.title, self.treeName)
    
    ## Generates an MD5 hash for this object to identify this variation.
    #  Only hashes the tree name itself, not the name or the title
    @property
    def md5( self ):
        return hashlib.md5( self.treeName ).hexdigest() 
        
    @property
    def treeName( self ):
        return self.__treeName
    
    @treeName.setter
    def treeName( self, value ):
        self.__treeName = value
        
    @property
    def isTreeSystematics( self ):
        return True

## Class that represents a variation of the weight expression
class WeightSystematicVariation( SystematicVariation ):
    ## Default constructor
    #  @param name                unique identifier (avoid spaces)
    #  @param title               title used for example in legends (use TLatex, optional, default uses name)
    #  @param weightExpression    weight expression used for this variation
    def __init__( self, name, title='', weightExpression='' ):
        SystematicVariation.__init__( self, name, title )
        self.weightExpression = weightExpression
    
    def __hash__( self ):
        # only hashes the weight expression
        return hash( self.weightExpression )
    
    def __eq__( self, other ):
        # compares only the weight expression
        return isinstance( other, WeightSystematicVariation ) and self.weightExpression == other.weightExpression
    
    def __repr__( self ):
        return 'WeightSystematicVariation(%r, %r, %r)' % (self.name, self.title, self.weightExpression)
    
    ## Generates an MD5 hash for this object to identify this variation.
    #  Only hashes the weight expression itself, not the name or the title
    @property
    def md5( self ):
        return hashlib.md5( self.weightExpression ).hexdigest() 
    
    @property
    def weightExpression( self ):
        return self.__weightExpression
    
    @weightExpression.setter
    def weightExpression( self, value ):
        self.__weightExpression = value
        
    @property
    def isWeightSystematics( self ):
        return True

#class for variations where both, weightExpressions and SumOfWeights need to change simultaneously
class SumOfWeightsSystematicVariation( SystematicVariation ):
    
    def __init__( self, name, title='', weightExpression='', metadataHistogram='h_metadata', metadataBin='8'):
        ## Default constructor
        #  @param name                unique identifier (avoid spaces)
        #  @param title               title used in legends (use TLatex)
        #  @param weightExpression    weight expression applied for this variation
        SystematicVariation.__init__( self, name, title )
        self.weightExpression = weightExpression
        self.metadataHistogram = metadataHistogram
        self.metadataBin = metadataBin
    
    def __hash__( self ):
        ## only hashes the weight expression
        #420
        hashExpression=self.weightExpression+self.metadataHistogram+self.metadataBin
        return hash( hashExpression )
    
    def __eq__( self, other ):
        ## compares only the weight expression
        sameType=isinstance( other, SumOfWeightsSystematicVariation)
        if not sameType:
            return False
        sameExpression=bool(self.weightExpression == other.weightExpression)
        sameMetadataHist=bool(self.metadataHistogram == other.metadataHistogram)
        sameMetadataBin=bool(self.metadataBin == other.metadataBin)
        return sameExpression and sameMetadataHist and sameMetadataBin 
    
    @property
    def md5( self ):
        ## generates an MD5 hash for this object, only hashes the weight expression
        #420
        hashExpression=self.weightExpression+self.metadataHistogram+self.metadataBin
        return hashlib.md5( hashExpression ).hexdigest() 
    
    @property
    def weightExpression( self ):
        return self.__weightExpression
    
    @weightExpression.setter
    def weightExpression( self, value ):
        self.__weightExpression = value
    
    @property
    def metadataHistogram( self ):
        return self.__metadataHistogram
    
    @weightExpression.setter
    def metadataHistogram( self, value ):
        self.__metadataHistogram = value
    
    @property
    def metadataBin( self ):
        return self.__metadataBin
    
    @weightExpression.setter
    def metadataBin( self, value ):
        self.__metadataBin = value
        
    @property
    def isSumOfWeightSystematics( self ):
        return True
        
## Class describing a systematic uncertainty applied to a Dataset.
#  It contains SystematicVariation objects for nominal, up and down variations.
#  Several variables allow to control the use of this systematics for histograms,
#  etc. Two lists of Cut objects control if this systematics applies to a certain
#  region: if Systematics::applyOnlyToRegions is filled, only regions explicitely
#  listed will apply this systematics. If Systematics::applyNotToRegions contains
#  the requested Cut, this systematics will not be applied to that histogram.
class Systematics( object ):
    ## Default constructor
    #  @param name                unique identifier (avoid spaces)
    #  @param title               title used in legends (use TLatex)
    #  @param nominal             SystematicVariation used as nominal
    #  @param up                  SystematicVariation used as up variation
    #  @param down                SystematicVariation used as down variation
    def __init__( self, name, title='', nominal=None, up=None, down=None):
        ## String describing the name (avoid white spaces)
        self.name = name
        ## String describing the title used for example in legend entries (use TLatex)
        self.title = title if title else self.name
        ## SystematicVariation used for nominal case
        self.nominal = nominal
        ## SystematicVariation used for up case
        self.up = up
        ## SystematicVariation used for down case
        self.down = down
        ## List of Cut objects that define regions where this systematics applies
        self.applyOnlyToRegions = []
        ## List of Cut objects that define regions where this systematics does not apply
        self.applyNotToRegions = []
        ## Decide wether the variation should be symmetrized when plotting
        self.symmetrize = False
    
    def __hash__( self ):
        # hashes the combination of nominal, up and down variations
        return hash( ( hash(self.nominal), hash(self.up), hash(self.down) ) )
    
    def __eq__( self, other ):
        # compares only the weight expression
        return isinstance( other, Systematics ) and self.nominal == other.nominal and self.up == other.up and self.down == other.down
    
    def __repr__( self ):
        return 'Systematics(%r, %r, %r, %r, %r)' % (self.name, self.title, self.nominal, self.up, self.down)
    
    ## Checks if this Systematics applies to the given region.
    #  @param cut     Cut defining the region of interest
    #  @return bool
    def appliesToRegion( self, cut ):
        if cut in self.applyNotToRegions or (self.applyOnlyToRegions and not cut in self.applyOnlyToRegions):
            return False
        return True
    
    ## Generates an MD5 hash for this object to identify this variation.
    #  Combines the MD5 hashes of the nominal, up and down variations.
    @property
    def md5( self ):
        md5 = hashlib.md5()
        md5.update( self.nominal.md5 )
        md5.update( self.up.md5 )
        md5.update( self.down.md5 )
        return md5.hexdigest()
    
    @property
    def up( self ):
        if self._up:
            return self._up
        return self.nominal
    
    @up.setter
    def up( self, systematicVariation ):
        if systematicVariation:
            systematicVariation.systematics = self
        self._up = systematicVariation
    
    @property
    def down( self ):
        if self._down:
            return self._down
        return self.nominal
    
    @down.setter
    def down( self, systematicVariation ):
        if systematicVariation:
            systematicVariation.systematics = self
        self._down = systematicVariation
    
    ## Constructor for a tree systematics.
    #  Creates the three associated TreeSystematicVariation objects for nominal,
    #  up and down variations. They are defined by the respective tree name to
    #  be used.
    #  @param name                unique identifier (avoid spaces)
    #  @param title               title used in legends (use TLatex)
    #  @param upTreeName          tree name used for the up variation
    #  @param downTreeName        tree name used for the down variation
    #  @param nominalTreeName     tree name used for the nominal variation
    #  @return Systematics object
    @classmethod
    def treeSystematics( cls, name, title='', upTreeName=None, downTreeName=None, nominalTreeName=None ):
        ## Automatically create a full set of tree systematics based on the different tree names
        title = title if title else name
        up = None
        if upTreeName:
            up = TreeSystematicVariation( name + '_high', title + ' (up)', upTreeName )
        down = None
        if downTreeName:
            down = TreeSystematicVariation( name + '_low', title + ' (down)', downTreeName )
        nominal = TreeSystematicVariation( name + '_nominal', title + ' (nominal)', nominalTreeName )
        return cls( name, title, nominal, up, down )
    
    ## Constructor for a weight systematics.
    #  Creates the three associated WeightSystematicVariation objects for nominal,
    #  up and down variations. They are defined by the respective weight expressions
    #  to be used.
    #  @param name                     unique identifier (avoid spaces)
    #  @param title                    title used in legends (use TLatex)
    #  @param upWeightExpression       weight expression used for the up variation
    #  @param downWeightExpression     weight expression used for the down variation
    #  @param nominalWeightExpression  weight expression used for the nominal variation
    #  @return Systematics object
    @classmethod
    def weightSystematics( cls, name, title='', upWeightExpression=None, downWeightExpression=None, nominalWeightExpression='1' ):
        title = title if title else name
        up = None
        if upWeightExpression:
            up = WeightSystematicVariation( name + '_high', title + ' (up)', upWeightExpression )
        down = None
        if downWeightExpression:
            down = WeightSystematicVariation( name + '_low', title + ' (down)', downWeightExpression )
        nominal = WeightSystematicVariation( name + '_nominal', title + ' (nominal)', nominalWeightExpression )
        return cls( name, title, nominal, up, down )
    
    ## Constructor for a scale systematics.
    #  Creates the three associated ScaleSystematicVariation objects for nominal,
    #  up and down variations. They are defined by the respective scale factors
    #  to be used.
    #  @param name                     unique identifier (avoid spaces)
    #  @param title                    title used in legends (use TLatex)
    #  @param upScale                  scale factor used for the up variation
    #  @param downScale                scale factor used for the down variation
    #  @param nominalScale             scale factor used for the nominal variation
    #  @return Systematics object
    @classmethod
    def scaleSystematics( cls, name, title='', upScale=1.0, downScale=1.0, nominalScale=1.0 ):
        title = title if title else name
        up = ScaleSystematicVariation( name + '_high', title + ' (up)', upScale )
        down = ScaleSystematicVariation( name + '_low', title + ' (down)', downScale )
        nominal = ScaleSystematicVariation( name + '_nominal', title + ' (nominal)', nominalScale )
        return cls( name, title, nominal, up, down )
    
    @classmethod
    def sumOfWeightsSystematics( cls, name, title='', upWeightExpression=None, metadataHistogram=None, metadataBin=None, nomWeightExpression='1',nomMetadataHistogram='h_metadat', nomMetadataBin='8' ):
        ## Automatically create a full set of sum of weight systematics based on the the different weightexpressions and metadata parameters
        title = title if title else name
        up = None
        if upWeightExpression:
            up = SumOfWeightsSystematicVariation( name=name + '_high', title= title + ' (up)', weightExpression=upWeightExpression, metadataHistogram=metadataHistogram, metadataBin=metadataBin )
        nominal=SumOfWeightsSystematicVariation( name=name + '_high', title= title + ' (up)', weightExpression=nomWeightExpression, metadataHistogram=nomMetadataHistogram, metadataBin=nomMetadataBin )
        down=nominal
        return cls( name, title, nominal, up, down )
    
    @property
    def isOneSided( self ):
        return self.down == self.nominal or self.up == self.nominal
    
    @property
    def isScaleSystematics( self ):
        ## Check if this is a scale systematics
        return self.nominal.isScaleSystematics or self.up.isScaleSystematics or self.down.isScaleSystematics
    
    @property
    def isShapeSystematics( self ):
        ## Check if this is a shape systematics
        return self.nominal.isShapeSystematics or self.up.isShapeSystematics or self.down.isShapeSystematics
    
    @property
    def isTreeSystematics( self ):
        ## Check if this is a tree systematics
        return self.nominal.isTreeSystematics or self.up.isTreeSystematics or self.down.isTreeSystematics
    
    @property
    def isWeightSystematics( self ):
        ## Check if this is a weight systematics
        return self.nominal.isWeightSystematics or self.up.isWeightSystematics or self.down.isWeightSystematics
    
    @property
    def isSumOfWeightSystematics( self ):
        ## Check if this is a weight systematics
        return self.nominal.isSumOfWeightSystematics or self.up.isSumOfWeightSystematics or self.down.isSumOfWeightSystematics

## Class to describe a set Systematics objects.
#  Provides functionality to retrieve total weight expressions, total scale
#  factors, etc. for all systematics in the set. Derived from python set.
class SystematicsSet( set ):
    ## Default constructor (copied from set constructor)
    def __init__( self, iterable=None ):
        self._data = {}
        if iterable is not None:
            self._update(iterable)
    
    # override add method from python set
    def add( self, obj ):
        if isinstance( obj, Systematics ):
            set.add( self, obj )
            
    # overide _update method from python set
    def _update( self, iterable ):
        for obj in iterable:
            self.add( obj )
    
    ## Create a SystematicsSet of all systematics that apply to the given region.
    #  Does not include any Systematics that does not apply to the given region.
    #  @param cut      Cut defining the region of interest (optional)
    #  @return SystematicsSet of all systematics applying to the given region
    def getSystematicsSetAppliesToRegion( self, cut ):
        result = SystematicsSet()
        for systematics in self:
            if systematics.appliesToRegion( cut ):
                result.add( systematics )
        return result
    
    ## Get a systematics from the set based on its name
    # @param name  name of the systematics to retrieve
    def getSystematic(self, name):
        for systematics in self:
            if systematics.name == name:
                return systematics
        # if name not found
        return None

    ## Create a SystematicsSet of all scale systematics in this set.
    #  @return SystematicsSet of all scale systematics
    @property
    def scaleSystematics( self ):
        result = SystematicsSet()
        for systematics in self:
            if systematics.isScaleSystematics:
                result.add( systematics )
        return result
    
    ## Create a SystematicsSet of all shape systematics in this set.
    #  @return SystematicsSet of all shape systematics
    @property
    def shapeSystematics( self ):
        result = SystematicsSet()
        for systematics in self:
            if systematics.isShapeSystematics:
                result.add( systematics )
        return result
    
    ## Create a SystematicsSet of all tree systematics in this set.
    #  @return SystematicsSet of all tree systematics
    @property
    def treeSystematics( self ):
        result = SystematicsSet()
        for systematics in self:
            if systematics.isTreeSystematics:
                result.add( systematics )
        return result
    
    ## Create a SystematicsSet of all weight systematics in this set.
    #  @return SystematicsSet of all weight systematics
    @property
    def weightSystematics( self ):
        result = SystematicsSet()
        for systematics in self:
            if systematics.isWeightSystematics:
                result.add( systematics )
        return result
    
    ## Create a SystematicsSet of all sum of weights systematics in this set.
    #  @return SystematicsSet of all sum of weights systematics
    @property
    def sumOfWeightsSystematics( self ):
        result = SystematicsSet()
        for systematics in self:
            if systematics.isSumOfWeightSystematics:
                result.add( systematics )
        return result
            
    ## Create a set of all tree names connected to any systematics in the list.
    #  @return set of tree names
    @property 
    def treeNames( self ):
        treeNames = set()
        for systematics in self:
            for variation in [systematics.nominal, systematics.up, systematics.down]:
                if variation.treeName:
                    treeNames.add( variation.treeName )
        return treeNames
    
    ## Collect all relevant nominal SystematicVariation objects in the set.
    #  By default, all nominal variations are included in the set. If a SystematicVariation is passed that
    #  one is used instead of the corresponding nominal variation. The cut is required to check wether each
    #  Systematics actually applies to the region of interest.
    #  @param systematicVariation     SystematicVariation replacing the corresponding nominal (optional)
    #  @param cut                     Cut defining the region of interest (optional)
    #  @return set of all relevant SystematicVariation objects   
    def getNominalVariations( self, systematicVariation=None, cut=Cut() ):
        variationsSet = set()
        for systematics in self:
            if systematics.appliesToRegion( cut ):
                variationsSet.add( systematics.nominal )
        # replace the corresponding nominal variation with the concrete variation
        if systematicVariation:
            systematics = systematicVariation.systematics
            if systematics and systematics.appliesToRegion( cut ):
                variationsSet.discard( systematics.nominal )
                variationsSet.add( systematicVariation )
        return variationsSet
    
    ## Calculates the combined weight expression of all systematics in the set.
    #  By default, all systematics use their nominal weight expression. If a SystematicVariation is passed that
    #  one is used instead of the corresponding nominal variation. The cut is required to check wether each
    #  Systematics actually applies to the region of interest.
    #  @param systematicVariation     SystematicVariation replacing the corresponding nominal (optional)
    #  @param cut                     Cut defining the region of interest (optional)
    #  @return total weight expression    
    def totalWeight( self, systematicVariation=None, cut=Cut() ):
        weightExpression = Cut( '' )
        for variation in self.getNominalVariations( systematicVariation, cut ):
            weightExpression *= variation.weightExpression
        return weightExpression
    
    ## Calculates the total scale factor of all systematics in the set.
    #  By default, all systematics use the nominal scale factor. If a SystematicVariation is passed that
    #  one is used instead of the corresponding nominal variation. The cut is required to check wether each
    #  Systematics actually applies to the region of interest.
    #  @param systematicVariation     SystematicVariation replacing the corresponding nominal (optional)
    #  @param cut                     Cut defining the region of interest (optional)
    #  @return total scale factor
    def totalScaleFactor( self, systematicVariation=None, cut=Cut() ):
        scaleFactor = 1.
        for variation in self.getNominalVariations( systematicVariation, cut ):
            scaleFactor *= variation.scale
        return scaleFactor
    
# Replace inherited methods from set that return a new set with methods that return SystematicsSet instead.
for methodName in ['__ror__', 'difference_update', '__isub__', 
    'symmetric_difference', '__rsub__', '__and__', '__rand__',
    'intersection', 'difference', '__iand__', 'union', '__ixor__', 
    'symmetric_difference_update', '__or__', 'copy', '__rxor__',
    'intersection_update', '__xor__', '__ior__', '__sub__',]:
    wrapMethod( SystematicsSet, set, methodName )


## Default nominal systematics used for n-tuples created by the xTauFramework
nominalSystematics = TreeSystematicVariation( 'nominal', 'Nominal', 'NOMINAL' )    
    
if __name__ == '__main__':
    s = SystematicsSet()
    s.add( Systematics.weightSystematics( 'testWeight1', 'Test Weight 1', 'upWeight1', 'downWeight1', 'nominalWeight' ) )
    s.add( Systematics.weightSystematics( 'testWeight2', 'Test Weight 2', 'upWeight2', 'downWeight2', 'nominalWeight' ) )
    s.add( Systematics.treeSystematics( 'testTree', 'Test Tree', 'upTree', 'downTree', 'nominalTree' ) )
    s.add( Systematics.scaleSystematics( 'testScale1', 'Test Scale 1', 1.31, 0.95, 1.08 ) )
    s.add( Systematics.scaleSystematics( 'testScale2', 'Test Scale 2', 1.13, 0.89, 1.02 ) )
    s.add( Systematics.sumOfWeightsSystematics( 'testSOWSyst', 'Test SOWSyst 2', 'theory_weight', 'h_metadata', '8' ))
    
    for ss in s:
        variation = ss.up
        print variation, '; totalWeightExpression:', s.totalWeight( variation ).cut, '; totalScaleFactor:', s.totalScaleFactor( variation )
