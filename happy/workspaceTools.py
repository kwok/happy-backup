## @package happy.workspaceTools
#  @brief Module to facilitate Dataset creation
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
import logging

class WorkspaceFactory:
    logger = logging.getLogger( __name__ + '.WorkspaceFactory' )
    ## Default constructor.
    #  @param title        title used for the measurement object
    #  @param luminosity   integrated luminosity in pb-1
    def __init__( self, title='measurement', luminosity=1. ):
        from ROOT import RooStats
        ## String
        self.title = title
        ## RooStats.HistFactory.Measurement object
        self.measurement = RooStats.HistFactory.Measurement( title, title )
        self.luminosity = luminosity
        
    ## Create a RooWorkspace from the Measurement.
    #  Add all channels before creating the workspace.
    def createWorkspace( self ):
        from ROOT import RooStats
        RooStats.HistFactory.MakeModelAndMeasurementFast( self.measurement )
        factory = RooStats.HistFactory.HistoToWorkspaceFactoryFast()
        return factory.MakeCombinedModel( self.measurement )
    
    ## Write histograms to a ROOT file.
    #  Consistent with what WorspaceBuilder expects.
    #  @param fileName    path of the output file
    def writeWorkspaceBuilderInput( self, fileName ):
        from ROOT import TFile
        outputFile = TFile.Open( fileName, 'recreate' )
        self.measurement.writeToFile( outputFile )
        outputFile.Close()
    
    ## Adds a channel to the measurement.
    #  A channel represents a distribution of an observable in a
    #  given event selection. The processes are Dataset objects that
    #  can contain data, signal and background, identified by the
    #  isSignal and isData flags.
    #  @param var         Variable object that defines the observable
    #  @param cut         Cut that represents the phase space selection
    #  @param processes   list of Dataset objects relevant for this channel
    def addChannel( self, var, cut, processes ):
        from ROOT import RooStats
        channel = RooStats.HistFactory.Channel( cut.name )
        self.logger.debug( 'Adding channel %r to %s' % (cut.name, self.title) )
        for process in processes:
            self.logger.debug( 'Adding sample %r' % process.name )
            hNom = process.getHistogram( var, cut, self.luminosity )
            hNom.SetNameTitle( process.nominalSystematics.name, '%s (%s)' % (process.title, process.nominalSystematics.title) )
            # treat data separately
            if process.isData:
                channel.SetStatErrorConfig( 0.05, 'Poisson' )
                channel.SetData( hNom )
                continue
            sample = RooStats.HistFactory.Sample( process.name )
            sample.ActivateStatError()
            # process all registered systematics
            for systematic in process.combinedSystematicsSet():
                if systematic.isScaleSystematics:
                    self.logger.debug( 'Adding OverallSys %r' % systematic.name )
                    sample.AddOverallSys( systematic.name, systematic.down.scale / systematic.nominal.scale, systematic.up.scale / systematic.nominal.scale )
                else:
                    self.logger.debug( 'Adding HistoSys %r' % systematic.name )
                    hLow = process.getHistogram( var, cut, self.luminosity, systematic.down )
                    hLow.SetNameTitle( systematic.down.name, '%s (%s)' % (process.title, systematic.down.title) )
                    hHigh = process.getHistogram( var, cut, self.luminosity, systematic.up )
                    hHigh.SetNameTitle( systematic.up.name, '%s (%s)' % (process.title, systematic.up.title) )
                    histoSys = RooStats.HistFactory.HistoSys( systematic.name )
                    histoSys.SetHistoDown( hLow )
                    histoSys.SetHistoHigh( hHigh )
                    sample.AddHistoSys( histoSys )
            channel.AddSample( sample )