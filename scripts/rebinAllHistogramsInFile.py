'''
Created on Apr 19, 2018

@author: cgrefe
'''
import argparse
from happy.core.variable import Variable, Binning
from happy.tools.histogramTools import rebinIntoNewHist, overflowIntoLastBins
import logging

def rebinHistogramsInDirectory(directory, outputFile, variable):
    import ROOT
    directoryPath = directory.GetPath().split(':')[1][1:]
    logging.info('Rebinning histograms in "%s"' % directoryPath)
    for key in directory.GetListOfKeys():
        className = key.GetClassName()
        classObj = ROOT.gROOT.GetClass(className)
        keyName = key.GetName()
        if classObj.InheritsFrom(ROOT.TDirectory.Class()):
            # get all plots from nested directory recursively
            rebinHistogramsInDirectory(directory.Get(keyName), outputFile, variable)
        else:
            hist = directory.Get(keyName)
            if isinstance(hist, ROOT.TH1):
                histCopy = rebinIntoNewHist(hist, variable)
                overflowIntoLastBins(histCopy)
                histCopy.SetDirectory(0)
                histCopy.SetNameTitle(hist.GetName(), hist.GetTitle())
                histCopy.GetXaxis().SetTitle(hist.GetXaxis().GetTitle())
                histCopy.GetYaxis().SetTitle(hist.GetYaxis().GetTitle())
                if not (outputFile.Get(directoryPath)):
                    outputFile.mkdir(directoryPath)
                outputFile.cd(directoryPath)
                histCopy.Write(histCopy.GetName())
                directory.cd()

def main(inputFileName, outputFileName, nBins, low, high):
    import ROOT
    variable = Variable('myVariable', binning=Binning(nBins, low, high))
    logging.info('Reading input histograms from "%s"' % inputFileName)
    logging.info('Writing output histograms to "%s"' % outputFileName)
    logging.info('Output histogram binning: N=%d, low=%s, high=%s' % (nBins, low, high))
    inputFile = ROOT.TFile.Open(inputFileName)
    outputFile = ROOT.TFile.Open(outputFileName, 'recreate')
    rebinHistogramsInDirectory(inputFile, outputFile, variable)
    inputFile.Close()
    outputFile.Close()    

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Script to rebin all histograms in a ROOT file and store them in another ROOT file with the same structure.')
    parser.add_argument('-i', '--input', help='Define the input workspace', required = True)
    parser.add_argument('-o', '--output', help='Output path for workpace', required = True)
    parser.add_argument('-n', '--bins', help='Number of bins', type=int)
    parser.add_argument('-d', '--down', help='Number of bins', type=float)
    parser.add_argument('-u', '--up', help='Number of bins', type=float)
    res = parser.parse_args()
    main(res.input, res.output, res.bins, res.down, res.up)