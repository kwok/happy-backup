#!/bin/bash

# check where HAPPy is installed (works regardless from where this script was actually executed)
export HAPPyDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Setting up HAPPy in ${HAPPyDIR} ..."
# add to python path
if [ -z "${PYTHONPATH}" ]; then
    export PYTHONPATH=${HAPPyDIR}
else
    export PYTHONPATH=${HAPPyDIR}:${PYTHONPATH}
fi
